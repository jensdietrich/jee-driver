package giga.jee.mock.javax.el;

import javax.el.FunctionMapper;
import java.lang.reflect.Method;

/**
 * Mock object.
 * @author jens dietrich
 */
public class MockFunctionMapper extends FunctionMapper {

    @Override
    public void mapFunction(String prefix, String localName, Method meth) {
        super.mapFunction(prefix, localName, meth);
    }

    @Override
    public Method resolveFunction(String s, String s1) {
        return null;
    }
}
