package giga.jee.mock.javax.servlet.jsp;

import giga.jee.mock.javax.servlet.MockSingletonEnumeration;
import javax.el.ELContext;
import javax.servlet.jsp.JspContext;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.el.ExpressionEvaluator;
import javax.servlet.jsp.el.VariableResolver;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */

public class MockJspContext extends JspContext {

    private Map<String,Object> attributes = new HashMap<String,Object>();

    public void setAttribute(String s, Object o) {
        this.attributes.put(s,o);
    }

    public void setAttribute(String s, Object o, int i) {
        this.attributes.put(s,o);
    }

    public Object getAttribute(String s) {
        return this.attributes.get(s);
    }

    public Object getAttribute(String s, int i) {
        return this.attributes.get(s);
    }

    public Object findAttribute(String s) {
        return this.attributes.get(s);
    }

    public void removeAttribute(String s) {
        this.attributes.remove(s);
    }

    public void removeAttribute(String s, int i) {
        this.attributes.remove(s);
    }

    public int getAttributesScope(String s) {
        return 0;
    }

    public Enumeration<String> getAttributeNamesInScope(int i) {
        return new MockSingletonEnumeration("mock attribute name");
    }

    public JspWriter getOut() {
        return new MockJspWriter();
    }

    // @TODO  deeper mocking

    public ExpressionEvaluator getExpressionEvaluator() {
        return null;
    }

    public VariableResolver getVariableResolver() {
        return null;
    }

    public ELContext getELContext() {
        return null;
    }
}
