package giga.jee.mock.javax.servlet;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import java.io.IOException;

public class MockServletInputStream extends ServletInputStream {

    public boolean isFinished() {
        return false;
    }

    public boolean isReady() {
        return false;
    }

    public void setReadListener(ReadListener readListener) {
    }

    public int read() throws IOException {
        return 0;
    }
}
