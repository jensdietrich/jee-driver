package giga.jee.driver;

import com.google.common.base.Preconditions;
import com.google.common.collect.Sets;
import giga.jee.driver.clhier.ClassHierarchy;
import giga.jee.driver.util.IOUtils;
import giga.jee.driver.util.LogSystem;
import org.apache.commons.io.FileUtils;
import org.apache.jasper.JasperException;
import org.apache.jasper.JspC;
import org.apache.log4j.Logger;
import javax.servlet.jsp.JspFactory;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Extract entry points from JSP pages.
 * Pages are precompiled using jasper (or more precisely, the jspc ant task).
 * @author jens dietrich
 */
public class JSPEntryPointExtractor implements EntryPointExtractor {

    private static Logger LOGGER = LogSystem.getLogger(JSPEntryPointExtractor.class);
    private static final String MOCK_JSP_FACTORY_INSTANTIATION = MockConfiguration.MOCKS.get(JspFactory.class.getName());

    @Override
    public Result extractEntryPoints(File webapp,Map<String,Object> parameters) {
        Preconditions.checkArgument(webapp.exists(),"File does not exist: " + webapp.getAbsolutePath());
        Preconditions.checkArgument(webapp.getName().endsWith(".war"),"File is not a war file: " + webapp.getAbsolutePath());
        ClassHierarchy hierarchy = (ClassHierarchy) parameters.get(DriverCompiler.TYPE_HIERARCHY);
        Preconditions.checkArgument(hierarchy!=null);

        LOGGER.info("Extracting entry points");

        Collection<EntryPoint> entryPoints = new LinkedHashSet<>();
        Map<File,String> jsps = new LinkedHashMap<>();

        // extract
        LOGGER.info("\tExtracting JSP pages");
        File tmpJSP = IOUtils.DEFAULT.createTmpDir("__"+ JSPEntryPointExtractor.class.getName() + "__JSP-extraction__"+ webapp.getName());
        try (ZipFile zip = new ZipFile(webapp)) {
            for (Enumeration<? extends ZipEntry> iter = zip.entries(); iter.hasMoreElements(); ) {
                ZipEntry next = iter.nextElement();
                String name = next.getName();
                if (!next.isDirectory()) {
                    if (name.endsWith(".jsp")) {
                        File jsp = new File(tmpJSP, next.getName());
                        LOGGER.info("\tExtracting JSP to: " + jsp);
                        jsp.getParentFile().mkdirs();
                        Files.copy(zip.getInputStream(next), Paths.get(jsp.getAbsolutePath()));
                        jsps.put(jsp, next.getName());
                    } else if (name.startsWith("WEB-INF")) {
                        File tld = new File(tmpJSP, name);
                        LOGGER.info("\tExtracting TLD to: " + tld);
                        tld.getParentFile().mkdirs();
                        Files.copy(zip.getInputStream(next), Paths.get(tld.getAbsolutePath()));
                    }
                }
            }
        }
        catch (IOException x) {
            LOGGER.error("\tError accessing war file " + webapp.getAbsolutePath(),x);
        }
        LOGGER.info("\tExtracted " + jsps.size() + " JSP pages");

        // copy additional libraries into web-inf/lib that are usually supplied by server
        // in particular jstl !!
        List<File> containerJars = (List<File>) parameters.get(DriverCompiler.CONTAINER_JARS);
        if (containerJars==null || containerJars.isEmpty()) {
            LOGGER.warn("No container jars found");
        }
        else {
            LOGGER.warn("Copy container jars to be used by jspc");
            for (File jar:containerJars) {
                File dest = new File(tmpJSP,"WEB-INF/lib/" + jar.getName());
                dest.getParentFile().mkdirs();
                try {
                    if (!dest.exists()) {
                        Files.copy(Paths.get(jar.getAbsolutePath()), Paths.get(dest.getAbsolutePath()));
                        LOGGER.info("Copied container jar " + dest.getAbsolutePath());
                    }
                }
                catch (IOException x) {
                    LOGGER.error("Error  copying container jar",x);
                }
            }
        }

        // compile
        // TODO: set classpaths
        LOGGER.info("\tCompiling JSP pages");
        File tmpServletSrc = IOUtils.DEFAULT.createTmpDir("__"+ JSPEntryPointExtractor.class.getName() + "__jasper__" + webapp.getName());
        JspC compiler = new JspC(){
            @Override
            protected void processFile(String file) throws JasperException {
                try {
                    super.processFile(file);
                    LOGGER.info("Processed: " + file);
                }
                catch (Exception x) {
                    LOGGER.error("Error processing " + file,x);
                    // swallow exception in order to continue processing !
                }
            }
        };
        compiler.setOutputDir(tmpServletSrc.getAbsolutePath());
        compiler.setUriroot(tmpJSP.getAbsolutePath());
        compiler.setPackage("giga.jee.driver.generated.jasper");
        compiler.setFailOnError(true);
        //compiler.setCompile(true);

        // validation may lead to network requests to access schemas, switch off !!
        compiler.setValidateTld(false);
        compiler.setValidateXml(false);


        try {
            compiler.execute();
        }
        catch (Exception x) {
            LOGGER.info("Error compiling JSPs with jasper",x);
        }
        LOGGER.info("\tCompiled JSP pages into servlets");

        LOGGER.info("\tPostprocessing generated servlets");
        int counter = 0;
        for (File servlet: FileUtils.listFiles(tmpServletSrc,new String[]{"java"},true)) {
            if (postprocessGeneratedServlet(servlet)) {
                counter = counter + 1;
            }
        }
        LOGGER.info("\tPostprocessed " + counter + "/" + jsps.size() + " generated servlets");

        // collecting class names
        Map<File,String> classNames = new LinkedHashMap<>();
        for (File servlet: FileUtils.listFiles(tmpServletSrc,new String[]{"java"},true)) {
            String className = extractClassName(servlet);
            if (className!=null) {
                String location = webapp.getName() + "//" + servlet.getName().replace(".java","").replace("_",".");
                EntryPoint.InvocationSpec invSpec = new EntryPoint.InvocationSpec("_jspService",ServletAPISpecs.SERVLET_METHOD_PARAMS);
                EntryPoint entryPoint = new EntryPoint(this,location,className,invSpec);
                entryPoints.add(entryPoint);
            }
            else {
                LOGGER.warn("\tError extracting full class name from " + servlet.getAbsolutePath());
            }
        }

        return new Result(
            Collections.unmodifiableCollection(entryPoints),
            Collections.unmodifiableCollection(Sets.newHashSet(tmpServletSrc))
        );
    }

    private String extractClassName(File servlet) {
        // simple string match based, could use javaparser
        // TODO code could fail when in multi-line comments
        String packageName = "";
        try {
            for (String line : Files.readAllLines(Paths.get(servlet.getAbsolutePath()))) {
                line = line.trim();
                if (line.startsWith("package ")) {
                    assert packageName.length()==0;
                    assert !line.contains("//");
                    line = line.substring("package ".length());
                    line = line.replace(";","");
                    line = line.trim();
                    packageName = line;
                }
                if (line.startsWith("public final class ")) {
                    line = line.substring("public final class ".length());
                    int pos = line.indexOf(" ");
                    line = line.substring(0,pos);
                    if (packageName.length()==0) {
                        return line;
                    }
                    else {
                        return packageName + "." + line;
                    }
                }

            }

        }
        catch (IOException x) {
            LOGGER.error("\tError postprocessing " + servlet.getAbsolutePath());
            return null;
        }

        return null;
    }

    private boolean postprocessGeneratedServlet(File servlet) {
        StringWriter transformedCode = new StringWriter();
        PrintWriter out = new PrintWriter(transformedCode);
        boolean transformationPerformed = true;

        try {
            for (String line : Files.readAllLines(Paths.get(servlet.getAbsolutePath()))) {
                if (line.contains("javax.servlet.jsp.JspFactory.getDefaultFactory();")) {
                    // remove final declaration
                    String newLine = line.replace("javax.servlet.jsp.JspFactory.getDefaultFactory()", MOCK_JSP_FACTORY_INSTANTIATION);
                    out.println(newLine);
                    transformationPerformed = true;
                }
                else {
                    out.println(line);
                }
            }

            // now override
            PrintWriter fOut = new PrintWriter(new FileWriter(servlet));
            fOut.println(transformedCode.getBuffer().toString());
            fOut.close();
            return transformationPerformed;

        }
        catch (IOException x) {
            LOGGER.error("\tError postprocessing " + servlet.getAbsolutePath());
            return false;
        }

    }

    private void addEntryPoint(String location,Collection<EntryPoint> entryPoints,String className,String methodName,String[] argNames, String category) {
        EntryPoint entryPoint = new EntryPoint(this,location,className, methodName, argNames);
        LOGGER.info("\textracted entry point: " + entryPoint.getClassName() + "#" + methodName + " (" + category + ")");
        entryPoints.add(entryPoint);
    }

    private void addEntryPoint(String location,ClassHierarchy hierarchy,Collection<EntryPoint> entryPoints,String className,String interfaceName,String methodName,String[] argNames, String category) {

        if (hierarchy.isSubtypeOf(className,interfaceName)) {
            EntryPoint entryPoint = new EntryPoint(this,location,className, methodName, argNames);
            LOGGER.info("\textracted entry point: " + entryPoint.getClassName() + "#" + methodName + " (" + category + ")");
            entryPoints.add(entryPoint);
        }
    }

}
