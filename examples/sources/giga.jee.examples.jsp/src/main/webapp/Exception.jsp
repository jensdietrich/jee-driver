<%@page contentType="text/html" isErrorPage="true"%>
<html>
<head><title>Error</title></head>
<body>

<h3>Error</h3>
<% application.log("Exception",exception);%>
An error occurred when accessing <%= request.getRequestURI()%>. The error has been logged and the 
web master has been informed.<p>
<h3>Exception Details:</h3><p>
<tt>
<% exception.printStackTrace(new java.io.PrintWriter(out)); %>

<% application.log("something went wrong", exception); %>
</tt>
</body>
</html>
