package giga.jee.mock.javax.servlet;

import giga.jee.mock.java.io.MockWriter;

import javax.servlet.ServletOutputStream;
import javax.servlet.ServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Locale;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */
public class MockServletResponse implements ServletResponse {

    public MockServletResponse() {
    }

    public String getCharacterEncoding() {
        return "UTF-8";
    }

    public String getContentType() {
        return "text/html-mocked";
    }

    public ServletOutputStream getOutputStream() throws IOException {
        return new MockServletOutputStream();
    }

    public PrintWriter getWriter() throws IOException {
        return new PrintWriter(new MockWriter());
    }

    public void setCharacterEncoding(String charset) {

    }

    public void setContentLength(int len) {

    }

    public void setContentLengthLong(long len) {

    }

    public void setContentType(String type) {

    }

    public void setBufferSize(int size) {

    }

    public int getBufferSize() {
        return 0;
    }

    public void flushBuffer() throws IOException {

    }

    public void resetBuffer() {

    }

    public boolean isCommitted() {
        return false;
    }

    public void reset() {

    }

    public void setLocale(Locale loc) {

    }

    public Locale getLocale() {
        return Locale.ENGLISH;
    }
}
