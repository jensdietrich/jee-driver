package giga.jee.mock.javax.el;

import javax.el.ELContext;
import javax.el.MethodExpression;
import javax.el.MethodInfo;

/**
 * Mock object.
 * @author jens dietrich
 */
public class MockMethodExpression extends MethodExpression {
    public MethodInfo getMethodInfo(ELContext elContext) {
        return new MethodInfo("",Object.class,new Class[]{});
    }

    public Object invoke(ELContext elContext, Object[] objects) {
        return "mock object";
    }

    public String getExpressionString() {
        return "mock expression string";
    }

    public boolean equals(Object o) {
        return false;
    }

    public int hashCode() {
        return 0;
    }

    public boolean isLiteralText() {
        return false;
    }
}
