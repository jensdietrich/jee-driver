package giga.jee.mock.java.io;

import java.io.IOException;
import java.io.Writer;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */

public class MockWriter extends Writer {
    public void write(char[] cbuf, int off, int len) throws IOException {
    }

    public void flush() throws IOException {
    }

    public void close() throws IOException {
    }
}
