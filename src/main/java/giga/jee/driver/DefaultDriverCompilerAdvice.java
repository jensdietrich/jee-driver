package giga.jee.driver;

import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicInteger;

import static giga.jee.driver.MockConfiguration.MOCKS;

/**
 * Default advice.
 * @author jens dietrich
 */
public class DefaultDriverCompilerAdvice implements DriverCompilerAdvice {

    public void insertCodeBeforeEachInvocation(PrintWriter out, EntryPoint.InvocationSpec spec, String offset) {}

    public void insertCodeAfterEachInvocation(PrintWriter out, EntryPoint.InvocationSpec spec, String offset) {}

    public void insertCodeBeforeAny(PrintWriter out,AtomicInteger varCounter, String offset) {
        out.println();
        out.println(offset + "java.sql.DriverManager.registerDriver(" + MOCKS.get(java.sql.Driver.class.getName()) + ");");
        out.println();
    }

    public void insertCodeAfterAll(PrintWriter out,AtomicInteger varCounter, String offset) {}
}
