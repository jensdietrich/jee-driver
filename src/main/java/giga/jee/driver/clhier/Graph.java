package giga.jee.driver.clhier;

import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Simple data structure to represent a graph.
 * We prefer this to JUNG graphs as
 * JUNG graphs do not permit multiple edges between the same source and target vertices.
 * @author jens dietrich
 */
public class Graph<V extends Vertex<E>,E extends Edge<V>> implements Serializable {
	
	private Map<String,V> index = new ConcurrentHashMap<String, V>();
    private static Logger LOGGER = Logger.getLogger(Graph.class);

	public boolean add(V v) {
		V old = index.put(v.getName(), v);
        if (old!=null) {
            LOGGER.debug("Replacing vertex " + old + " with vertex with same name in index" + v);
        }

		return old==null;
	}

    public boolean add(E e) {
        return e.getStart().addOutEdge(e) &&
                e.getEnd().addInEdge(e);
    }

    public void redirectSink(E e,V newSink) {
        V oldSink = e.getEnd();
        if (newSink!=oldSink) {
            oldSink.removeInEdge(e);
            e.end = newSink;
            newSink.addInEdge(e);
        }
    }

    public void redirectSource(E e,V newSource) {
        V oldSource = e.getStart();
        if (newSource!=oldSource) {
            oldSource.removeOutEdge(e);
            e.start = newSource;
            newSource.addOutEdge(e);
        }
    }

    public boolean remove(E e) {
        assert e.getStart()!=null;
        assert e.getEnd()!=null;
        return e.getStart().removeOutEdge(e) & e.getEnd().removeInEdge(e);
    }

	public boolean remove(V v) {

//        for (E e:v.getInEdges()) {
//            success = success & e.getStart().removeOutEdge(e);
//        }
//        for (E e:v.getOutEdges()) {
//            success = success & e.getEnd().removeInEdge(e);
//        }

        // to avoid conc. modification of sets
        Collection<E> edgesToRemove = new HashSet<>();
        edgesToRemove.addAll(v.getInEdges());
        edgesToRemove.addAll(v.getOutEdges());

        boolean success = true;
        for (E e:edgesToRemove) {
            success = success & remove(e);
        }

        success = index.remove(v.getName())!=null;

		assert success;
		return success;
	}
	
	public V getVertexByName(String name) {
		return this.index.get(name);
	}

    public V getOrAdd(String name, Function<? super String,? extends V> factory) {
        return index.computeIfAbsent(name,factory);
    }
	
	public Collection<V> getVertices() {
		return Collections.unmodifiableCollection(index.values());
	}

    @Deprecated  // collect is slow, better to filter at client
    public Collection<V> getVertices(Predicate<V> filter) {
		return index.values().stream().filter(filter).collect(Collectors.toList());
	}

	public Collection<V> getVertices(Class type) {
		return getVertices(v->(type.isAssignableFrom(v.getClass())));
	}

	public Collection<E> getEdges() {
		Set<E> edges = new HashSet<E>(index.values().size()*3);
		for (V v:index.values()) {
			edges.addAll((Collection<? extends E>) v.getOutEdges());
		}
		return edges;
	}

    @Deprecated  // collect is slow, better to filter at client
	public Collection<E> getEdges(Predicate<E> filter) {
		return getEdges().stream().filter(filter).collect(Collectors.toSet());
	}

    public Collection<E> getEdges(Class<?> type) {
        return getEdges(e->(type.isAssignableFrom(e.getClass())));
    }

	public Stream<V> vertices(boolean parallel) {
        return parallel?index.values().parallelStream():index.values().stream();
	}

    @Deprecated // trivial logic, can be easily done at callsites
	public Stream<V> vertices(Predicate<V> filter) {
		return index.values().stream().filter(filter);
	}

	public Stream<E> edges(boolean parallel) {
        return vertices(parallel).flatMap(v -> v.getOutEdges().stream());
	}

	public long getVertexCount() {
		return this.index.values().size();
	}
    public long getEdgeCount() {
        return edges(false).count();
    }
	
}
