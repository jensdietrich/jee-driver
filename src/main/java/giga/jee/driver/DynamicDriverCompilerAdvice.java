package giga.jee.driver;

import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicInteger;

import static giga.jee.driver.MockConfiguration.MOCKS;

/**
 * Advice that guards each invocation with a try-catch, and maintains some stats.
 * @author jens dietrich
 */
public class DynamicDriverCompilerAdvice extends DefaultDriverCompilerAdvice {

    @Override
    public void insertCodeBeforeEachInvocation(PrintWriter out, EntryPoint.InvocationSpec spec, String offset) {
        out.print(offset);
        out.println("try {");
    }

    @Override
    public void insertCodeAfterEachInvocation(PrintWriter out, EntryPoint.InvocationSpec spec, String offset) {
        out.print(offset);
        out.println("\tokCount++;");
        out.print(offset);
        out.println("} catch (Throwable _t) {");
        out.print(offset);
        out.println("\terrorCount++;");
        out.print(offset);
        out.println("}");
    }

    @Override
    public void insertCodeBeforeAny(PrintWriter out,AtomicInteger varCounter,String offset) {
        super.insertCodeBeforeAny(out,varCounter,offset);
        out.print(offset);
        out.println("int okCount = 0;");
        out.print(offset);
        out.println("int errorCount = 0;");
    }

    @Override
    public void insertCodeAfterAll(PrintWriter out, AtomicInteger varCounter,String offset) {
        super.insertCodeBeforeAny(out,varCounter,offset);
        out.print(offset);
        out.println("System.out.println(\"ok: \" + okCount);");
        out.print(offset);
        out.println("System.out.println(\"error: \" + errorCount);");
    }
}
