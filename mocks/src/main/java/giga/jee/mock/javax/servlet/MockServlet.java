package giga.jee.mock.javax.servlet;

import javax.servlet.*;
import java.io.IOException;

public class MockServlet implements Servlet {

    private ServletConfig servletConfig = null;

    public void init(ServletConfig servletConfig) throws ServletException {
        this.servletConfig = servletConfig;
    }

    public ServletConfig getServletConfig() {
        return servletConfig;
    }

    public void service(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
    }

    public String getServletInfo() {
        return "mock servlet info";
    }

    public void destroy() {
    }
}
