package giga.jee.mock.org.springframework.extensions.config;

import org.springframework.extensions.config.ServerProperties;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */
public class MockServerProperties implements ServerProperties {

    public String getScheme() {
        return "mock scheme";
    }

    public String getHostName() {
        return "mock host";
    }

    public Integer getPort() {
        return 80;
    }
}
