package giga.jee.mock.javax.servlet;

import giga.jee.mock.java.io.MockInputStream;

import javax.servlet.*;
import javax.servlet.descriptor.JspConfigDescriptor;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */
public class MockFilterConfig implements FilterConfig {

    private Map<String,String> initParameters = new HashMap<String, String>();
    private ServletContext context = new MockServletContext();

    public MockFilterConfig() {
    }

    public String getFilterName() {
        return "mock filter";
    }

    public ServletContext getServletContext() {
        return context;
    }

    public String getInitParameter(String s) {
        return this.initParameters.get(s);
    }

    public Enumeration<String> getInitParameterNames() {
        return Collections.enumeration(this.initParameters.keySet());
    }
}
