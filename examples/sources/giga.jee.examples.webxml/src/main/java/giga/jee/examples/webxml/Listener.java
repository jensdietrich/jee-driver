package giga.jee.examples.webxml;

import javax.servlet.http.*;

/**
 * Listener that uses web.xml for JEE configuration.
 * @author jens dietrich
 */
public class Listener implements HttpSessionListener {


    public void sessionCreated(HttpSessionEvent se) {

    }
    public void	sessionDestroyed(HttpSessionEvent se) {

    }

    private void foo() {
        System.out.println("Session listener called");
    }

}
