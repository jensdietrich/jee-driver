package giga.jee.mock.javax.el;

import javax.el.ELContext;
import javax.el.ELResolver;
import java.beans.FeatureDescriptor;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Mock object.
 * @author jens dietrich
 */

public class MockELResolver extends ELResolver {

    public Object getValue(ELContext elContext, Object o, Object o1) {
        return "mock value";
    }

    public Class<?> getType(ELContext elContext, Object o, Object o1) {
        return Object.class;
    }

    public void setValue(ELContext elContext, Object o, Object o1, Object o2) {

    }

    public boolean isReadOnly(ELContext elContext, Object o, Object o1) {
        return false;
    }

    public Iterator<FeatureDescriptor> getFeatureDescriptors(ELContext elContext, Object o) {
        FeatureDescriptor f = new java.beans.FeatureDescriptor();
        List<FeatureDescriptor> list = new ArrayList();
        list.add(f);
        return list.iterator();
    }

    public Class<?> getCommonPropertyType(ELContext elContext, Object o) {
        return Object.class;
    }
}
