## A Driver Generator for the Static Analysis of JEE Applications

###Synopsis

Many static analyses depend on a driver that provides an entry point for program execution, such as a `main` method. 
The program to be analysed is explored from this entry point. 
For instance, a statically constructed call graph only contains methods reachable from such an entry point. 

The purpose of this utility is to generate such an entry point for applications implemented using the *Java EE web profile* (now part of [EE4J](https://projects.eclipse.org/projects/ee4j/)).
The driver generator creates a class `Driver` with a static `main` method that serves as a single JEE application entry point, simulating clients interacting with the application. 
There are surprisingly many ways code in JEE applications can be invoked from the outside (i.e., from the container responding to incoming network requests), the most common ones are certain methods servlets, filters and listeners mapped to URLs by means of annotations or entries in  `web.xml`.  

The driver generator scans a web application represented as a `war` file for configuration files, annotations and patterns, extract the respective entry points and generates a driver class with a `main` method that invokes these methods. 
This class is then compiled and packaged as a jar, although the source code is made available as well. Often, this requires certain parameters to be instantiated by the container. For instance, in order to invoke a servlets `doGet()` method, a request and a response object are needed as parameters. In a real-world deployment scenario, those objects are being supplied by the container like tomcat. Therefore, the driver needs mocks to simulate the container. Existing dynamic mocking techniques such as mockito are not suitable here as static analysis tools tend to have problems to cope with dynamic language features like proxies. 
Therefore, we provide static mock objects, the respective sources and a build script can be found in the `mocks` folder.

The driver generator is designed to be *platform-agnostic*, i.e. it can be used as a pre-analysis with many static analysers. 

###Limitations and How to Add Additional Extractors

The entry point extraction itself is modular and plugin-in based. To add a new extractor plugin, one must implement the interface `giga.jee.driver.EntryPointExtractor`, and make sure that the implementing class is registered in the meta data of its jar. 
The driver generator uses the simple `java.util.ServiceLoader` mechanism to organise plugins. 
Implementing a plugin may also require the creation of additional mocks, the  respective additional mock jar can be passed to the driver generator as parameter.

Please check the [issues](https://bitbucket.org/jensdietrich/jee-driver/issues/) for a discussion of 
missing and planned features.

###Building and Running The Driver Generator

####Prerequisites

The following programs must be in the path:

1. `hg`  -- mercurial client to check out the project
2. `mvn` -- maven to build the project from sources
3. `javac` and `jar` -- JDK 8 (or better) binaries to compile Java sources, and build jars. Note that those binaries are used by the 
driver generator by launching OS commands via `Runtime.exec()`

####Then perform the following steps:

1. check out the code with `hg clone https://bitbucket.org/jensdietrich/jee-driver`
2. build the project with maven: `mvn clean compile`
3. collect dependencies: `mvn dependency:copy-dependencies`
4. run `java -cp target/classes:target/dependencies/* giga.jee.driver.DriverCompiler <args>`

Alternatively,  a single jar file containing all dependencies can be build with `mvn assembly:single` and then be  started 
with `java -jar jee-driver-<version>-jar-with-dependencies.jar`.

###Runtime Parameters

Required runtime parameters will be displayed when the driver generator is started with the `-help` option. 
The driver generator needs the locations of several jars: the jar containing the mocks, the jar containing the JEE API and the core JRE jars. The `lib` folder has the respective binaries that can be used as defaults. 
If you want to use a particular version or a custom mock jar, just use alternatives. 


###Examples
mvn dependency:copy-dependencies
There are three examples in the `examples` folder - the first two are web applications with a servlet and a web listener, declared with
annotations and `web.xml` entries. The folders contain pre-built wars, and the respective sources and build scripts. In order to generate the driver for one of the examples, 
execute the following script after following the build instructions above from the project root folder:

`java -cp target/classes:target/dependencies/* giga.jee.driver.DriverCompiler -war examples/example-annotated.war -out out`

There is a third example with JSP pages. Additional, some large real-world web applications packaged as wars that can be used to test the driver generator can be downloaded from [here](https://my.pcloud.com/publink/show?code=kZpAdj7ZUFlbxDPDjFFzuncJpsSEVSPVwyLX).

###Related Projects

[Xanitizer](https://www.rigs-it.com/xanitizer/) is a commercial static analyser that supports multiple JEE-related frameworks. Its focus is on finding vulnerabilities, it has a number of built-in detectors for vulnerabilities from the OWASP Top 10 list. Xanitizer has an integrated driver generator similar to this tool. 

###License

The project is licensed under the [The Universal Permissive License (UPL)](https://opensource.org/licenses/UPL). This project is supported by Oracle Labs, Australia.

Copyright (c) 2018 Jens Dietrich

