package giga.jee.mock.org.springframework.extensions.webscripts.servlets;

import giga.jee.mock.org.springframework.extensions.webscripts.MockAuthenticator;
import org.springframework.extensions.webscripts.Authenticator;
import org.springframework.extensions.webscripts.servlet.ServletAuthenticatorFactory;
import org.springframework.extensions.webscripts.servlet.WebScriptServletRequest;
import org.springframework.extensions.webscripts.servlet.WebScriptServletResponse;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */
public class MockServletAuthenticatorFactory implements ServletAuthenticatorFactory {
    public Authenticator create(WebScriptServletRequest webScriptServletRequest, WebScriptServletResponse webScriptServletResponse) {
        return new MockAuthenticator();
    }
}
