package giga.jee.mock.java.io;

import java.io.IOException;
import java.io.Reader;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */

public class MockReader extends Reader {
    public int read(char[] cbuf, int off, int len) throws IOException {
        return 0;
    }

    public void close() throws IOException {
    }
}
