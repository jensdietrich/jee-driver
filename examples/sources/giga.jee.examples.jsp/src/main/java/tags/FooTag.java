package tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.sql.*;
import static javax.servlet.jsp.tagext.Tag.*;

/**
 * A SQL select tag. Note that the DB settings and the actual SQL query are hardcoded. 
 * This is just to illustrate the principles.
 * @author  jens dietrich
 */
public class FooTag extends TagSupport {

    public FooTag() {
        super();
    }

    public void otherDoStartTagOperations()  {
        try {
            JspWriter out = pageContext.getOut();
            out.print("foo!");
        }
        catch (Exception ex) { }
    }


    public boolean theBodyShouldBeEvaluated()  {
        return true;
    }

    public void otherDoEndTagOperations()  {
    }

    public boolean shouldEvaluateRestOfPageAfterEndTag()  {
        return true;
    }

    public int doStartTag() throws JspException {
        otherDoStartTagOperations();
        if (theBodyShouldBeEvaluated()) {
            return EVAL_BODY_INCLUDE;
        } else {
            return SKIP_BODY;
        }
    }

    public int doEndTag() throws JspException {
        otherDoEndTagOperations();
        if (shouldEvaluateRestOfPageAfterEndTag()) {
            return EVAL_PAGE;
        } else {
            return SKIP_PAGE;
        }
    }
}
