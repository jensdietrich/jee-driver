package giga.jee.mock.javax.servlet.http;

import giga.jee.mock.java.io.MockWriter;
import giga.jee.mock.javax.servlet.MockServletOutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */
public class MockHttpServletResponse implements HttpServletResponse {

    public MockHttpServletResponse() {
    }

    private Map<String,String> headers = new HashMap<String,String>();
    private List<Cookie> cookies = new ArrayList<Cookie>();

    public void addCookie(Cookie cookie) {
        this.cookies.add(cookie);
    }

    public boolean containsHeader(String name) {
        return this.headers.containsKey(name);
    }

    public String encodeURL(String url) {
        return url+"/mock-encoded-url";
    }

    public String encodeRedirectURL(String url) {
        return url+"/mock-redirect-url";
    }

    public String encodeUrl(String url) {
        return url+"/mock-encoded-url";
    }

    public String encodeRedirectUrl(String url) {
        return url+"/mock-redirect-url";
    }

    public void sendError(int sc, String msg) throws IOException {
    }

    public void sendError(int sc) throws IOException {
    }

    public void sendRedirect(String location) throws IOException {
    }

    public void setDateHeader(String name, long date) {
        this.headers.put(name,new Long(date).toString());
    }

    public void addDateHeader(String name, long date) {
        this.headers.put(name,new Long(date).toString());
    }

    public void setHeader(String name, String value) {
        this.headers.put(name,value);
    }

    public void addHeader(String name, String value) {
        this.headers.put(name,value);
    }

    public void setIntHeader(String name, int value) {
        this.headers.put(name,new Integer(value).toString());
    }

    public void addIntHeader(String name, int value) {
        this.headers.put(name,new Integer(value).toString());
    }

    public void setStatus(int sc) {
    }

    public void setStatus(int sc, String sm) {

    }
    public int getStatus() {
        return 0;
    }

    public String getHeader(String name) {
        return this.headers.get(name);
    }

    public Collection<String> getHeaders(String name) {
        List<String> l = new ArrayList<String>();
        l.add(this.headers.get(name));
        return l;
    }

    public Collection<String> getHeaderNames() {
        return this.headers.keySet();
    }

    public String getCharacterEncoding() {
        return "UTF-8";
    }

    public String getContentType() {
        return "text/html-mocked";
    }

    public ServletOutputStream getOutputStream() throws IOException {
        return new MockServletOutputStream();
    }

    public PrintWriter getWriter() throws IOException {
        return new PrintWriter(new MockWriter());
    }

    public void setCharacterEncoding(String charset) {

    }

    public void setContentLength(int len) {

    }

    public void setContentLengthLong(long len) {

    }

    public void setContentType(String type) {

    }

    public void setBufferSize(int size) {

    }

    public int getBufferSize() {
        return 0;
    }

    public void flushBuffer() throws IOException {

    }

    public void resetBuffer() {

    }

    public boolean isCommitted() {
        return false;
    }

    public void reset() {

    }

    public void setLocale(Locale loc) {

    }
    public Locale getLocale() {
        return Locale.ENGLISH;
    }
}
