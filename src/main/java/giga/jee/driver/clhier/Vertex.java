package giga.jee.driver.clhier;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.concurrent.ConcurrentHashMap;

/**
 * General vertex class. Vertices have references to incoming and outgoing edges. This is different to JUNG 2. 
 * The advantage is that lookup/navigation is faster.
 * TODO: add API to safely remove vertices and edges
 * @author jens dietrich
 */

public class Vertex<E extends Edge<?>> implements Serializable {

	private static final long serialVersionUID = 6383085786558591800L;
	
	private String name = null;
	private Collection<E> outEdges = Collections.newSetFromMap(new ConcurrentHashMap<>());
	private Collection<E> inEdges = Collections.newSetFromMap(new ConcurrentHashMap<>());

	public Vertex() {
		super();
	}
	public Vertex(String name) {
		super();
		this.name = name;
	}

	public Collection<E> getOutEdges() {
		return outEdges;
	}
	public Collection<E> getInEdges() {
		return inEdges;
	}

    public int getOutDegree() {
        return outEdges.size();
    }
    public int getInDegree() {
        return inEdges.size();
    }
    public int getDegree() {
        return this.getInDegree() + this.getOutDegree();
    }

	boolean addInEdge(Edge<?> e) {
		return this.inEdges.add((E) e);
	}
	boolean addOutEdge(Edge<?> e) {
		return this.outEdges.add((E) e);
	}

    boolean removeInEdge(Edge<?> e) {
        return this.inEdges.remove((E) e);
    }
    boolean removeOutEdge(Edge<?> e) {
        return this.outEdges.remove((E) e);
    }

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return "Vertex [" + name + "]";
	}

	public boolean isSuccessor(Vertex<?> v) {
		for (E e:this.outEdges) {
			if (e.end==v) return true;
		}
		return false;
	}

	public boolean isPredecessors(Vertex<?> v) {
		for (E e:this.inEdges) {
			if (e.start==v) return true;
		}
		return false;
	}

	public Vertex getOneSuccessor(Class<? extends Edge> edgeType) {
		for (E e:this.outEdges) {
			if (edgeType.isAssignableFrom(e.getClass())) return e.getEnd();
		}
		return null;
	}

	public Vertex getOnePredecessor(Class<? extends Edge> edgeType) {
		for (E e:this.inEdges) {
			if (edgeType.isAssignableFrom(e.getClass())) return e.getStart();
		}
		return null;
	}

	public Collection<Vertex> getSuccessors(Class<? extends Edge> edgeType) {
		Collection<Vertex> successors = new HashSet<>(this.outEdges.size());
		for (E e:this.outEdges) {
			if (edgeType.isAssignableFrom(e.getClass())) successors.add(e.getEnd());
		}
		return successors;
	}

	public Collection<Vertex> getPredecessors(Class<? extends Edge> edgeType) {
		Collection<Vertex> predecessors = new HashSet<>(this.inEdges.size());
		for (E e:this.inEdges) {
			if (edgeType.isAssignableFrom(e.getClass())) predecessors.add(e.getStart());
		}
		return predecessors;
	}

    public Collection<Vertex> getSuccessors() {
        Collection<Vertex> successors = new HashSet<>(this.outEdges.size());
        for (E e:this.outEdges) {
            successors.add(e.getEnd());
        }
        return successors;
    }

    public Collection<Vertex> getPredecessors() {
        Collection<Vertex> predecessors = new HashSet<>(this.inEdges.size());
        for (E e:this.inEdges) {
            predecessors.add(e.getStart());
        }
        return predecessors;
    }
}
