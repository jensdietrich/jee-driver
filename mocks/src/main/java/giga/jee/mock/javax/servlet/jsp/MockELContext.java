package giga.jee.mock.javax.servlet.jsp;

import giga.jee.mock.javax.el.MockFunctionMapper;
import giga.jee.mock.javax.el.MockVariableMapper;

import javax.el.*;

public class MockELContext extends ELContext {

    public ELResolver getELResolver() {
        return new MapELResolver();
    }

    public FunctionMapper getFunctionMapper() {
        return new MockFunctionMapper();
    }

    public VariableMapper getVariableMapper() {
        return new MockVariableMapper();
    }
}
