package giga.jee.mock.javax.servlet;

import javax.servlet.ServletOutputStream;
import javax.servlet.WriteListener;
import java.io.IOException;

public class MockServletOutputStream extends ServletOutputStream {
    public boolean isReady() {
        return false;
    }

    public void setWriteListener(WriteListener writeListener) {
    }

    public void write(int b) throws IOException {
    }
}
