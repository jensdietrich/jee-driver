package giga.jee.driver.util;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import giga.jee.driver.EntryPoint;
import giga.jee.driver.EntryPointExtractor;
import giga.jee.driver.clhier.ClassHierarchy;
import giga.jee.driver.clhier.ClassHierarchyBuilder;
import org.apache.commons.cli.*;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipFile;

import static giga.jee.driver.MockConfiguration.MOCKS;

/**
 * Utility to extract some statistics from a web application.
 * @author jens dietrich
 */
public class WarStats {

    private static Logger LOGGER = LogSystem.getLogger(WarStats.class);
    public static final String OPTION_WAR = "war";

    public static void main (String[] args) throws Exception {
        Options options = new Options();

        options.addOption(OPTION_WAR, true, "the location of the web application to be analysed(required)");
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(options, args);

        if (!cmd.hasOption(OPTION_WAR)) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java <options> " + WarStats.class.getName(), options);
            System.exit(0);
        }

        String warFileName = cmd.getOptionValue(OPTION_WAR);
        File war = new File(warFileName);
        Preconditions.checkArgument(war.exists(),"The war file does not exist: " + warFileName);

        List<File> jars = WarUnpacker.unpack(war);

        List<File> thirdPartyJars = jars.stream().filter(f -> !f.getName().equals(WarUnpacker.CLASSES_JAR)).collect(Collectors.toList());
        List<File> classesJars = Arrays.asList(war);

        assert jars.size() == thirdPartyJars.size() + classesJars.size();
        assert classesJars.size() == 1;

        System.out.println("classes: " + countClasses(classesJars));
        System.out.println("third-party jar files: " + thirdPartyJars.size());
        System.out.println("classes in third-party jar files: " + countClasses(thirdPartyJars));

    }

    private static int countClasses(List<File> jars) throws Exception {
        int count = 0;
        for (File jar : jars) {
            count = count + (int) new ZipFile(jar).stream()
                    .filter(entry -> !entry.isDirectory())
                    .filter(entry -> entry.getName().endsWith(".class"))
                    .count();
        }
        return count;
    }


}
