package giga.jee.driver;

/**
 * Extract entry points using servlet configurations in web.xml files.
 * @author jens dietrich
 */
public class ServletAPISpecs {

    // servlets

    public static final String SERVLET_METHOD_NAME = "service"; // note that methods like doGet in HttpServlet are not public ! , however service invokes them

    public static final String[] SERVLET_METHOD_PARAMS = {
            "javax.servlet.http.HttpServletRequest",
            "javax.servlet.http.HttpServletResponse"
    };

    public static final String SERVLET_INIT_METHOD_NAME1 = "init";
    public static final String SERVLET_INIT_METHOD_NAME2 = "init";
    public static final String SERVLET_DESTROY_METHOD_NAME = "destroy";

    public static final String[] SERVLET_INIT_METHOD_PARAMS1 = {};
    public static final String[] SERVLET_INIT_METHOD_PARAMS2 = {"javax.servlet.ServletConfig"};
    public static final String[] SERVLET_DESTROY_METHOD_PARAMS = {};

    // filters

    public static final String FILTER_METHOD_NAME = "doFilter"; // note that methods like doGet in HttpServlet are not public ! , however service invokes them

    public static final String[] FILTER_METHOD_PARAMS = {
            "javax.servlet.http.HttpServletRequest",
            "javax.servlet.http.HttpServletResponse",
            "javax.servlet.FilterChain"
    };

    public static final String FILTER_INIT_METHOD_NAME = "init";
    public static final String FIlTER_DESTROY_METHOD_NAME = "destroy";

    public static final String[] FILTER_INIT_METHOD_PARAMS = {"javax.servlet.FilterConfig"};
    public static final String[] FILTER_DESTROY_METHOD_PARAMS = {};

    // events

    // servlet context events
    public static final String SERVLET_CONTEXT_EVENT_METHOD_NAME1 = "contextDestroyed";
    public static final String SERVLET_CONTEXT_EVENT_METHOD_NAME2 = "contextInitialized";
    public static final String[] SERVLET_CONTEXT_EVENT_PARAMS = {
            "javax.servlet.ServletContextEvent"
    };

    // servlet context attribute listener
    public static final String SERVLET_CONTEXT_ATTRIBUTE_LISTENER_NAME1 = "attributeAdded";
    public static final String SERVLET_CONTEXT_ATTRIBUTE_LISTENER_NAME2 = "attributeRemoved";
    public static final String SERVLET_CONTEXT_ATTRIBUTE_LISTENER_NAME3 = "attributeReplaced";
    public static final String[] SERVLET_CONTEXT_ATTRIBUTE_LISTENER_PARAMS = {
            "javax.servlet.ServletContextAttributeEvent"
    };

    // session events
    public static final String SERVLET_SESSION_EVENT_METHOD_NAME1 = "sessionCreated";
    public static final String SERVLET_SESSION_EVENT_METHOD_NAME2 = "sessionDestroyed";
    public static final String[] SERVLET_SESSION_EVENT_PARAMS = {
            "javax.servlet.http.HttpSessionEvent"
    };

    // servlet session attribute listener
    public static final String SERVLET_SESSION_ATTRIBUTE_LISTENER_NAME1 = "attributeAdded";
    public static final String SERVLET_SESSION_ATTRIBUTE_LISTENER_NAME2 = "attributeRemoved";
    public static final String SERVLET_SESSION_ATTRIBUTE_LISTENER_NAME3 = "attributeReplaced";
    public static final String[] SERVLET_SESSION_ATTRIBUTE_LISTENER_PARAMS = {
            "javax.servlet.http.HttpSessionBindingEvent"
    };

    // request events
    public static final String SERVLET_REQUEST_EVENT_METHOD_NAME1 = "requestInitialized";
    public static final String SERVLET_REQUEST_EVENT_METHOD_NAME2 = "requestDestroyed";
    public static final String[] SERVLET_REQUEST_EVENT_PARAMS = {
            "javax.servlet.ServletRequestEvent"
    };

    // servlet request attribute listener
    public static final String SERVLET_REQUEST_ATTRIBUTE_LISTENER_NAME1 = "attributeAdded";
    public static final String SERVLET_REQUEST_ATTRIBUTE_LISTENER_NAME2 = "attributeRemoved";
    public static final String SERVLET_REQUEST_ATTRIBUTE_LISTENER_NAME3 = "attributeReplaced";
    public static final String[] SERVLET_REQUEST_ATTRIBUTE_LISTENER_PARAMS = {
            "javax.servlet.ServletRequestAttributeEvent"
    };

}
