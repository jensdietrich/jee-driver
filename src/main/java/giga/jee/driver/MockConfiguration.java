package giga.jee.driver;

import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.JspContext;
import javax.servlet.jsp.JspFactory;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.tagext.BodyContent;
import javax.servlet.jsp.tagext.JspFragment;
import java.sql.Driver;
import java.util.HashMap;
import java.util.Map;

/**
 * Associates (abstract) types with their respective mock implementations.
 * @author jens dietrich
 */
public class MockConfiguration {


    public static final Map<String,String> MOCKS = new HashMap() {
        {

            // standard JEE
            put(HttpServletRequest.class.getName(),     "new giga.jee.mock.javax.servlet.http.MockHttpServletRequest()");
            put(HttpServletResponse.class.getName(),    "new giga.jee.mock.javax.servlet.http.MockHttpServletResponse()");
            put(ServletRequest.class.getName(),         "new giga.jee.mock.javax.servlet.MockServletRequest()");
            put(ServletResponse.class.getName(),        "new giga.jee.mock.javax.servlet.MockServletResponse()");
            put(HttpSession.class.getName(),            "new giga.jee.mock.javax.servlet.http.MockHttpSession()");
            put(FilterChain.class.getName(),            "new giga.jee.mock.javax.servlet.MockFilterChain()");

            put(ServletContextEvent.class.getName(),                "new giga.jee.mock.javax.servlet.MockServletContextEvent()");
            put(ServletContextAttributeEvent.class.getName(),       "new giga.jee.mock.javax.servlet.MockServletContextAttributeEvent()");
            put(HttpSessionEvent.class.getName(),                   "new giga.jee.mock.javax.servlet.http.MockHttpSessionEvent()");
            put(HttpSessionBindingEvent.class.getName(),            "new giga.jee.mock.javax.servlet.http.MockHttpSessionBindingEvent()");
            put(ServletRequestEvent.class.getName(),                "new giga.jee.mock.javax.servlet.MockServletRequestEvent()");
            put(ServletRequestAttributeEvent.class.getName(),       "new giga.jee.mock.javax.servlet.MockServletRequestAttributeEvent()");

            put(ServletConfig.class.getName(),                      "new giga.jee.mock.javax.servlet.MockServletConfig()");
            put(FilterConfig.class.getName(),                       "new giga.jee.mock.javax.servlet.MockFilterConfig()");

            //jsp
            put(JspContext.class.getName(),     "new giga.jee.mock.javax.servlet.jsp.MockJspContext()");
            put(PageContext.class.getName(),    "new giga.jee.mock.javax.servlet.jsp.MockPageContext()");
            put(JspFragment.class.getName(),    "new giga.jee.mock.javax.servlet.jsp.tagext.MockJspFragment()");
            put(BodyContent.class.getName(),    "new giga.jee.mock.javax.servlet.jsp.tagext.MockBodyContent()");
            put(JspFactory.class.getName(),    "new giga.jee.mock.javax.servlet.jsp.MockJspFactory()");

            // spring web scripts
            put(WebScriptResponse.class.getName(),  "new giga.jee.mock.org.springframework.extensions.webscripts.MockWebScriptResponse()");
            put(WebScriptRequest.class.getName(),  "new giga.jee.mock.org.springframework.extensions.webscripts.MockWebScriptRequest()");

            // jdbc
            put(Driver.class.getName(),  "new giga.jee.mock.java.sql.MockDriver()");
        }
    };

}
