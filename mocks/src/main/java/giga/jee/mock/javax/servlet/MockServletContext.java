package giga.jee.mock.javax.servlet;

import giga.jee.mock.java.io.MockInputStream;

import javax.servlet.*;
import javax.servlet.descriptor.JspConfigDescriptor;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */
public class MockServletContext implements ServletContext {

    public MockServletContext() {
    }

    private Map<String,String> initParameters = new HashMap<String, String>();
    private Map<String,Object> attributes = new HashMap<String, Object>();

    public String getContextPath() {
        return null;
    }

    public ServletContext getContext(String s) {
        return this;
    }

    public int getMajorVersion() {
        return 0;
    }

    public int getMinorVersion() {
        return 0;
    }

    public int getEffectiveMajorVersion() {
        return 0;
    }

    public int getEffectiveMinorVersion() {
        return 0;
    }

    public String getMimeType(String s) {
        return null;
    }

    public Set<String> getResourcePaths(String s) {
        return new HashSet<String>();
    }

    public URL getResource(String s) throws MalformedURLException {
        return new URL("http://localhost/mock");
    }

    public InputStream getResourceAsStream(String s) {
        return new MockInputStream();
    }

    public RequestDispatcher getRequestDispatcher(String s) {
        return new MockRequestDispatcher();
    }

    public RequestDispatcher getNamedDispatcher(String s) {
        return new MockRequestDispatcher();
    }

    public Servlet getServlet(String s) throws ServletException {
        return new MockServlet();
    }

    public Enumeration<Servlet> getServlets() {
        return new MockSingletonEnumeration(new MockServlet());
    }

    public Enumeration<String> getServletNames() {
        return new MockSingletonEnumeration("mock servlet");
    }

    public void log(String s) {
    }

    public void log(Exception e, String s) {
    }

    public void log(String s, Throwable throwable) {
    }

    public String getRealPath(String s) {
        return null;
    }

    public String getServerInfo() {
        return null;
    }

    public String getInitParameter(String s) {
        return this.initParameters.get(s);
    }

    public Enumeration<String> getInitParameterNames() {
        return Collections.enumeration(this.initParameters.keySet());
    }

    public boolean setInitParameter(String s, String s1) {
        return this.initParameters.put(s,s1)==null;
    }

    public Object getAttribute(String s) {
        return this.attributes.get(s);
    }

    public Enumeration<String> getAttributeNames() {
        return Collections.enumeration(this.attributes.keySet());
    }

    public void setAttribute(String s, Object o) {
        this.attributes.put(s,o);
    }

    public void removeAttribute(String s) {
        this.attributes.remove(s);
    }

    public String getServletContextName() {
        return "mock servlet context name";
    }

    public ServletRegistration.Dynamic addServlet(String s, String s1) {
        return null;
    }

    public ServletRegistration.Dynamic addServlet(String s, Servlet servlet) {
        return null;
    }

    public ServletRegistration.Dynamic addServlet(String s, Class<? extends Servlet> aClass) {
        return null;
    }

    public ServletRegistration.Dynamic addJspFile(String s, String s1) {
        return null;
    }

    public <T extends Servlet> T createServlet(Class<T> aClass) throws ServletException {
        return null;
    }

    public ServletRegistration getServletRegistration(String s) {
        return null;
    }

    public Map<String, ? extends ServletRegistration> getServletRegistrations() {
        return new HashMap();
    }

    public FilterRegistration.Dynamic addFilter(String s, String s1) {
        return null;
    }

    public FilterRegistration.Dynamic addFilter(String s, Filter filter) {
        return null;
    }

    public FilterRegistration.Dynamic addFilter(String s, Class<? extends Filter> aClass) {
        return null;
    }

    public <T extends Filter> T createFilter(Class<T> aClass) throws ServletException {
        return null;
    }

    public FilterRegistration getFilterRegistration(String s) {
        return null;
    }

    public Map<String, ? extends FilterRegistration> getFilterRegistrations() {
        return new HashMap();
    }

    public SessionCookieConfig getSessionCookieConfig() {
        return null;
    }

    public void setSessionTrackingModes(Set<SessionTrackingMode> set) {

    }

    public Set<SessionTrackingMode> getDefaultSessionTrackingModes() {
        return null;
    }

    public Set<SessionTrackingMode> getEffectiveSessionTrackingModes() {
        return null;
    }

    public void addListener(String s) {

    }

    public <T extends EventListener> void addListener(T t) {

    }

    public void addListener(Class<? extends EventListener> aClass) {

    }

    public <T extends EventListener> T createListener(Class<T> aClass) throws ServletException {
        return null;
    }

    public JspConfigDescriptor getJspConfigDescriptor() {
        return null;
    }

    public ClassLoader getClassLoader() {
        return null;
    }

    public void declareRoles(String... strings) {

    }

    public String getVirtualServerName() {
        return "mock virtual server name";
    }

    public int getSessionTimeout() {
        return 0;
    }

    public void setSessionTimeout(int i) {

    }

    public String getRequestCharacterEncoding() {
        return "UTF-8";
    }

    public void setRequestCharacterEncoding(String s) {

    }

    public String getResponseCharacterEncoding() {
        return "UTF-8";
    }

    public void setResponseCharacterEncoding(String s) {

    }

}
