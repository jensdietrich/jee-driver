package giga.jee.mock.javax.servlet;

import giga.jee.mock.java.io.MockReader;

import javax.servlet.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.*;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */
public class MockServletRequest implements ServletRequest {

    public MockServletRequest() {
    }

    private Map<String,String> parameters = new HashMap<String, String>();
    private Map<String,Object> attributes = new HashMap<String,Object>();

    public Object getAttribute(String name) {
        return this.attributes.get(name);
    }
    public Enumeration<String> getAttributeNames() {
        return Collections.enumeration(this.attributes.keySet());
    }
    public String getCharacterEncoding() {
        return "UTF-8";
    }
    public void setCharacterEncoding(String env) throws UnsupportedEncodingException {

    }
    public int getContentLength() {
        return 0;
    }
    public long getContentLengthLong() {
        return 0;
    }
    public String getContentType() {
        return "text/html-mocked";
    }
    public ServletInputStream getInputStream() throws IOException {
        return new MockServletInputStream();
    }
    public String getParameter(String name) {
        return this.parameters.get(name);
    }
    public Enumeration<String> getParameterNames() {
        return Collections.enumeration(this.parameters.keySet());
    }
    public String[] getParameterValues(String name) {
        return this.parameters.values().toArray(new String[this.parameters.size()]);
    }
    public Map<String, String[]> getParameterMap() {
        return new HashMap<String, String[]>();
    }
    public String getProtocol() {
        return "mock protocol";
    }
    public String getScheme() {
        return "mock scheme";
    }
    public String getServerName() {
        return "mock server name";
    }
    public int getServerPort() {
        return 0;
    }
    public BufferedReader getReader() throws IOException {
        return new BufferedReader(new MockReader());
    }
    public String getRemoteAddr() {
        return "mock.remote.addr";
    }
    public String getRemoteHost() {
        return "mock.remote.host";
    }
    public void setAttribute(String name, Object o) {
        this.attributes.put(name,o);
    }
    public void removeAttribute(String name) {
        this.attributes.remove(name);
    }
    public Locale getLocale() {
        return Locale.ENGLISH;
    }
    public Enumeration<Locale> getLocales() {
        return new MockSingletonEnumeration(Locale.ENGLISH);
    }
    public boolean isSecure() {
        return false;
    }
    public RequestDispatcher getRequestDispatcher(String path) {
        return new MockRequestDispatcher();
    }
    public String getRealPath(String path) {
        return "/mock/real/path";
    }
    public int getRemotePort() {
        return 0;
    }
    public String getLocalName() {
        return "mock local name";
    }
    public String getLocalAddr() {
        return "mock-local-dir";
    }
    public int getLocalPort() {
        return 0;
    }
    public ServletContext getServletContext() {
        return new MockServletContext();
    }
    public AsyncContext startAsync() throws IllegalStateException {
        return new MockAsyncContext();
    }
    public AsyncContext startAsync(ServletRequest servletRequest, ServletResponse servletResponse) throws IllegalStateException {
        return new MockAsyncContext();
    }
    public boolean isAsyncStarted() {
        return false;
    }
    public boolean isAsyncSupported() {
        return false;
    }
    public AsyncContext getAsyncContext() {
        return new MockAsyncContext();
    }
    public DispatcherType getDispatcherType() {
        return DispatcherType.ASYNC;
    }
}
