package giga.jee.mock.java.security;

import java.security.Principal;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */

public class MockPrincipal implements Principal {

    public String getName() {
        return "mock principal";
    }
}
