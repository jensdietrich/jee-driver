package giga.jee.driver.clhier;


import giga.jee.driver.util.WarUnpacker;
import giga.jee.driver.util.LogSystem;
import org.apache.log4j.Logger;
import org.objectweb.asm.Opcodes;
import java.io.File;
import java.util.Collection;
import java.util.List;

/**
 * Utility to build a model of the class hierarchy from a war file to be used to generate a driver.
 * @author jens dietrich
 */
public class ClassHierarchyBuilder {

    public static final int ASM_VERSION = Opcodes.ASM5;
    private static final Logger LOGGER = LogSystem.getLogger(ClassHierarchyBuilder.class);

    public ClassHierarchy buildMetaModel(File war,List<File> jarsToResolve) throws Exception {
        List<File> jars = WarUnpacker.unpack(war);
        // jars.addAll(Environment.STANDARD_JARS_TO_ANALYSE);
        jars.addAll(jarsToResolve);
        return buildMetaModel(jars);
    }


    public ClassHierarchy buildMetaModel(Collection<File> jars) throws Exception {
        // LOGGER.info("Building meta model from jars " + jars.stream().limit(3).map(f->f.getName()).collect(Collectors.joining(",")) + (jars.size()>3?"..":""));
        ClassHierarchy metaModel = new ClassHierarchy(jars);
        return metaModel;
    }
}
