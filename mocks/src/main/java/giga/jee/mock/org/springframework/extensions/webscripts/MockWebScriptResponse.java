package giga.jee.mock.org.springframework.extensions.webscripts;

import giga.jee.mock.java.io.MockOutputStream;
import giga.jee.mock.java.io.MockWriter;
import org.springframework.extensions.webscripts.*;
import java.io.*;
import org.springframework.extensions.webscripts.Cache;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */
public class MockWebScriptResponse extends WebScriptResponseImpl {

    public MockWebScriptResponse() {
        super(new MockRuntime());
    }

    public String getEncodeScriptUrlFunction(String s) {
        return "foo";
    }

    public String encodeScriptUrl(java.lang.String s) {
        return s;
    }

    public void reset() {}

    public OutputStream getOutputStream() {
        return new MockOutputStream();
    }

    public Writer getWriter() {
        return new MockWriter();
    }

    public void setCache(Cache cache) {};

    public void setContentEncoding(String contentEncoding) {}

    public void setContentType(String contextType) {}

    public void setHeader(java.lang.String name,java.lang.String value) {}

    public void addHeader(java.lang.String name,java.lang.String value) {}

    public void setStatus(int s){}
}
