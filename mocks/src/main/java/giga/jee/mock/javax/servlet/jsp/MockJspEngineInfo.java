package giga.jee.mock.javax.servlet.jsp;

import javax.servlet.jsp.JspEngineInfo;

/**
 * Mock object for jsp engine info.
 * @author jens dietrich
 */
public class MockJspEngineInfo extends JspEngineInfo {

    public String getSpecificationVersion() {
        return "mock spec version";
    }
}
