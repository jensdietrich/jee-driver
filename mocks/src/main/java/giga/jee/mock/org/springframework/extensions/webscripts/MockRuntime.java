package giga.jee.mock.org.springframework.extensions.webscripts;

import giga.jee.mock.javax.servlet.http.MockHttpServletRequest;
import giga.jee.mock.javax.servlet.http.MockHttpServletResponse;
import giga.jee.mock.org.springframework.extensions.config.MockServerProperties;
import giga.jee.mock.org.springframework.extensions.webscripts.servlets.MockServletAuthenticatorFactory;
import org.springframework.extensions.webscripts.servlet.WebScriptServletRuntime;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */
public class MockRuntime extends WebScriptServletRuntime {

    public MockRuntime() {
        super(
            new MockRuntimeContainer(),
            new MockServletAuthenticatorFactory(),
            new MockHttpServletRequest(),
            new MockHttpServletResponse(),
            new MockServerProperties()
        );
    }

}
