package giga.jee.mock.javax.servlet.http;

import giga.jee.mock.javax.servlet.MockServletContext;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionContext;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */
public class MockHttpSession implements HttpSession {

    private ServletContext context = new MockServletContext();
    private Map<String,Object> attributes = new HashMap<String,Object>();

    public MockHttpSession() {
    }

    public long getCreationTime() {
        return 0;
    }

    public String getId() {
        return "mock session id";
    }

    public long getLastAccessedTime() {
        return 0;
    }

    public ServletContext getServletContext() {
        return context;
    }

    public void setMaxInactiveInterval(int interval) {

    }

    public int getMaxInactiveInterval() {
        return 0;
    }

    public HttpSessionContext getSessionContext() {
        return null;
    }

    public Object getAttribute(String name) {
        return this.attributes.get(name);
    }

    public Object getValue(String name) {
        return getAttribute(name);
    }

    public Enumeration<String> getAttributeNames() {
        return Collections.enumeration(this.attributes.keySet());
    }

    public String[] getValueNames() {
        return this.attributes.keySet().toArray(new String[this.attributes.size()]);
    }

    public void setAttribute(String name, Object value) {
        this.attributes.put(name,value);
    }

    public void putValue(String name, Object value) {
        this.setAttribute(name,value);
    }

    public void removeAttribute(String name) {
        this.removeAttribute(name);
    }

    public void removeValue(String name) {
        this.removeAttribute(name);
    }

    public void invalidate() {

    }

    public boolean isNew() {
        return false;
    }
}
