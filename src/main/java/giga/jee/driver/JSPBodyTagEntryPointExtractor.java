package giga.jee.driver;

import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import giga.jee.driver.clhier.ClassHierarchy;
import giga.jee.driver.util.LogSystem;
import org.apache.log4j.Logger;
import java.io.File;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * Extract entry points from tag (incl body tag) classes.
 * @see javax.servlet.jsp.tagext.SimpleTag
 * @author jens dietrich
 */
public class JSPBodyTagEntryPointExtractor implements EntryPointExtractor {

    private static Logger LOGGER = LogSystem.getLogger(JSPBodyTagEntryPointExtractor.class);
    private static final String BODY_TAG_SUPERTYPE = "javax.servlet.jsp.tagext.BodyTag";
    private static final String TAG_SUPERTYPE = "javax.servlet.jsp.tagext.Tag";

    @Override
    public Result extractEntryPoints(File webapp,Map<String,Object> parameters) {
        ClassHierarchy hierarchy = (ClassHierarchy) parameters.get(DriverCompiler.TYPE_HIERARCHY);
        Preconditions.checkArgument(hierarchy!=null);

        LOGGER.info("Extracting entry points");
        Collection<EntryPoint> entryPoints = new LinkedHashSet<>();
        ClassHierarchy.Type tagRoot = hierarchy.getVertexByName(TAG_SUPERTYPE);
        ClassHierarchy.Type bodyTagRoot = hierarchy.getVertexByName(BODY_TAG_SUPERTYPE);

        Preconditions.checkState(tagRoot!=null,"Root type for jsp tags not found: " + TAG_SUPERTYPE);
        Preconditions.checkState(bodyTagRoot!=null,"Root type for jsp body tags not found: " + BODY_TAG_SUPERTYPE);

        Predicate<ClassHierarchy.Type> isBodyTag = t -> hierarchy.isSubtypeOf(t,bodyTagRoot);
        addEntryPoints(webapp.getName(),entryPoints, tagRoot,isBodyTag);


        return new Result(entryPoints);
    }

    private void addEntryPoints(String warName,Collection<EntryPoint> entryPoints, ClassHierarchy.Type type, Predicate<ClassHierarchy.Type> isBodyTag) {



        if (!type.isAbstract() && type.isPublic() && !type.getName().startsWith("javax.")) {
            if (isBodyTag.test(type)) {
                entryPoints.add(new EntryPoint(this,warName+"/"+type.getName(),type.getName(),
                    new EntryPoint.InvocationSpec("setPageContext", "javax.servlet.jsp.PageContext"),
                    new EntryPoint.InvocationSpec("doStartTag"),
                    new EntryPoint.InvocationSpec("setBodyContent", "javax.servlet.jsp.tagext.BodyContent"),
                    new EntryPoint.InvocationSpec("doInitBody"),
                    new EntryPoint.InvocationSpec("doAfterBody"),
                    new EntryPoint.InvocationSpec("doEndTag"),
                    new EntryPoint.InvocationSpec("release")
                ));
            }
            else {
                entryPoints.add(new EntryPoint(this,warName+"/"+type.getName(),type.getName(),
                    new EntryPoint.InvocationSpec("setPageContext", "javax.servlet.jsp.PageContext"),
                    new EntryPoint.InvocationSpec("doStartTag"),
                    new EntryPoint.InvocationSpec("doEndTag"),
                    new EntryPoint.InvocationSpec("release")
                ));
            }
        }

        type.getInEdges().stream()
            .filter(e -> e instanceof ClassHierarchy.Subtype)
            .map(e -> e.getStart())
            .forEach(t -> addEntryPoints(warName,entryPoints,t,isBodyTag));
    }
}
