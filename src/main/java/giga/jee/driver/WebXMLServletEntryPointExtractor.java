package giga.jee.driver;

import com.google.common.base.Preconditions;
import giga.jee.driver.clhier.ClassHierarchy;
import giga.jee.driver.util.LogSystem;
import org.apache.log4j.Logger;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaders;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Extract entry points using servlet configurations in web.xml files.
 * @author jens dietrich
 */
public class WebXMLServletEntryPointExtractor implements EntryPointExtractor {

    private static Logger LOGGER = LogSystem.getLogger(WebXMLServletEntryPointExtractor.class);

    private static final String webxmlName = "WEB-INF/web.xml";

    @Override
    public Result extractEntryPoints(File webapp,Map<String,Object> parameters) {
        Preconditions.checkArgument(webapp.exists(),"File does not exist: " + webapp.getAbsolutePath());
        Preconditions.checkArgument(webapp.getName().endsWith(".war"),"File is not a war file: " + webapp.getAbsolutePath());

        ClassHierarchy hierarchy = (ClassHierarchy) parameters.get(DriverCompiler.TYPE_HIERARCHY);
        Preconditions.checkArgument(hierarchy!=null);

        LOGGER.info("Extracting entry points");

        Collection<EntryPoint> entryPoints = new LinkedHashSet<>();
        try (ZipFile zip = new ZipFile(webapp)) {

            ZipEntry webxml = zip.getEntry(webxmlName);
            Preconditions.checkArgument(webxml!=null,"Zip file does not contain " + webxmlName);
            try (InputStream in = zip.getInputStream(webxml)) {
                SAXBuilder builder = new SAXBuilder(XMLReaders.NONVALIDATING);
                builder.setValidation(false); // still get validation problems
                Document doc = builder.build(in);
                Element root = doc.getRootElement();
                assert root.getName().equals("web-app");

                String location1 = webapp.getName() + "/" + webxmlName + "//<servlet><servlet-class>";
                root.getChildren().stream()
                    .filter(e -> e.getName().equals("servlet"))
                    .forEach(
                        e -> {
                            e.getChildren().stream()
                            .filter(e2 -> e2.getName().equals("servlet-class"))
                            .forEach(e2 -> {
                                addEntryPoint(location1,entryPoints,e2.getValue(), ServletAPISpecs.SERVLET_INIT_METHOD_NAME1, ServletAPISpecs.SERVLET_INIT_METHOD_PARAMS1,"servlet");
                                addEntryPoint(location1,entryPoints,e2.getValue(), ServletAPISpecs.SERVLET_INIT_METHOD_NAME2, ServletAPISpecs.SERVLET_INIT_METHOD_PARAMS2,"servlet");
                                addEntryPoint(location1,entryPoints,e2.getValue(), ServletAPISpecs.SERVLET_METHOD_NAME, ServletAPISpecs.SERVLET_METHOD_PARAMS,"servlet");
                                addEntryPoint(location1,entryPoints,e2.getValue(), ServletAPISpecs.SERVLET_DESTROY_METHOD_NAME, ServletAPISpecs.SERVLET_DESTROY_METHOD_PARAMS,"servlet");
                            });
                        }
                    );

                String location2 = webapp.getName() + "/" + webxmlName + "//<filter><filter-class>";
                root.getChildren().stream()
                    .filter(e -> e.getName().equals("filter"))
                    .forEach(
                        e -> {
                            e.getChildren().stream()
                            .filter(e2 -> e2.getName().equals("filter-class"))
                            .forEach(e2 -> {
                                addEntryPoint(location2,entryPoints,e2.getValue(), ServletAPISpecs.FILTER_INIT_METHOD_NAME, ServletAPISpecs.FILTER_INIT_METHOD_PARAMS,"filter");
                                addEntryPoint(location2,entryPoints,e2.getValue(), ServletAPISpecs.FILTER_METHOD_NAME, ServletAPISpecs.FILTER_METHOD_PARAMS,"filter");
                                addEntryPoint(location2,entryPoints,e2.getValue(), ServletAPISpecs.FIlTER_DESTROY_METHOD_NAME, ServletAPISpecs.FILTER_DESTROY_METHOD_PARAMS,"filter");
                            });
                        }
                    );


                // TODO : this will create false entry points as a listener usually implements only some of those methods
                root.getChildren().stream()
                    .filter(e -> e.getName().equals("listener"))
                    .forEach(
                        e -> {
                            e.getChildren().stream()
                            .filter(e2 -> e2.getName().equals("listener-class"))
                            .forEach(e2 -> {
                                String className = e2.getValue();
                                String location3 = webapp.getName() + "/" + webxmlName + "//<listener><listener-class>";

                                addEntryPoint(location3,hierarchy,entryPoints,className,"javax.servlet.ServletContextListener",ServletAPISpecs.SERVLET_CONTEXT_EVENT_METHOD_NAME1, ServletAPISpecs.SERVLET_CONTEXT_EVENT_PARAMS,"listener");
                                addEntryPoint(location3,hierarchy,entryPoints,className, "javax.servlet.ServletContextListener",ServletAPISpecs.SERVLET_CONTEXT_EVENT_METHOD_NAME2, ServletAPISpecs.SERVLET_CONTEXT_EVENT_PARAMS,"listener");
                                addEntryPoint(location3,hierarchy,entryPoints,className, "javax.servlet.ServletContextAttributeListener",ServletAPISpecs.SERVLET_CONTEXT_ATTRIBUTE_LISTENER_NAME1, ServletAPISpecs.SERVLET_CONTEXT_ATTRIBUTE_LISTENER_PARAMS,"listener");
                                addEntryPoint(location3,hierarchy,entryPoints,className, "javax.servlet.ServletContextAttributeListener",ServletAPISpecs.SERVLET_CONTEXT_ATTRIBUTE_LISTENER_NAME2, ServletAPISpecs.SERVLET_CONTEXT_ATTRIBUTE_LISTENER_PARAMS,"listener");
                                addEntryPoint(location3,hierarchy,entryPoints,className, "javax.servlet.ServletContextAttributeListener",ServletAPISpecs.SERVLET_CONTEXT_ATTRIBUTE_LISTENER_NAME3, ServletAPISpecs.SERVLET_CONTEXT_ATTRIBUTE_LISTENER_PARAMS,"listener");

                                addEntryPoint(location3,hierarchy,entryPoints,className, "javax.servlet.http.HttpSessionListener",ServletAPISpecs.SERVLET_SESSION_EVENT_METHOD_NAME1, ServletAPISpecs.SERVLET_SESSION_EVENT_PARAMS,"listener");
                                addEntryPoint(location3,hierarchy,entryPoints,className, "javax.servlet.http.HttpSessionListener",ServletAPISpecs.SERVLET_SESSION_EVENT_METHOD_NAME2, ServletAPISpecs.SERVLET_SESSION_EVENT_PARAMS,"listener");
                                addEntryPoint(location3,hierarchy,entryPoints,className, "javax.servlet.http.HttpSessionAttributeListener",ServletAPISpecs.SERVLET_SESSION_ATTRIBUTE_LISTENER_NAME1, ServletAPISpecs.SERVLET_SESSION_ATTRIBUTE_LISTENER_PARAMS,"listener");
                                addEntryPoint(location3,hierarchy,entryPoints,className, "javax.servlet.http.HttpSessionAttributeListener", ServletAPISpecs.SERVLET_SESSION_ATTRIBUTE_LISTENER_NAME2, ServletAPISpecs.SERVLET_SESSION_ATTRIBUTE_LISTENER_PARAMS,"listener");
                                addEntryPoint(location3,hierarchy,entryPoints,className, "javax.servlet.http.HttpSessionAttributeListener", ServletAPISpecs.SERVLET_SESSION_ATTRIBUTE_LISTENER_NAME3, ServletAPISpecs.SERVLET_SESSION_ATTRIBUTE_LISTENER_PARAMS,"listener");

                                addEntryPoint(location3,hierarchy,entryPoints,className, "javax.servlet.ServletRequestListener",ServletAPISpecs.SERVLET_REQUEST_EVENT_METHOD_NAME1, ServletAPISpecs.SERVLET_REQUEST_EVENT_PARAMS,"listener");
                                addEntryPoint(location3,hierarchy,entryPoints,className, "javax.servlet.ServletRequestListener",ServletAPISpecs.SERVLET_REQUEST_EVENT_METHOD_NAME2, ServletAPISpecs.SERVLET_REQUEST_EVENT_PARAMS,"listener");
                                addEntryPoint(location3,hierarchy,entryPoints,className, "javax.servlet.ServletRequestAttributeListener",ServletAPISpecs.SERVLET_REQUEST_ATTRIBUTE_LISTENER_NAME1, ServletAPISpecs.SERVLET_REQUEST_ATTRIBUTE_LISTENER_PARAMS,"listener");
                                addEntryPoint(location3,hierarchy,entryPoints,className, "javax.servlet.ServletRequestAttributeListener",ServletAPISpecs.SERVLET_REQUEST_ATTRIBUTE_LISTENER_NAME2, ServletAPISpecs.SERVLET_REQUEST_ATTRIBUTE_LISTENER_PARAMS,"listener");
                                addEntryPoint(location3,hierarchy,entryPoints,className, "javax.servlet.ServletRequestAttributeListener",ServletAPISpecs.SERVLET_REQUEST_ATTRIBUTE_LISTENER_NAME3, ServletAPISpecs.SERVLET_REQUEST_ATTRIBUTE_LISTENER_PARAMS,"listener");

                            });
                        }
                    );


            }
            catch (JDOMException x) {
                LOGGER.error("Error parsing web.xml",x);
            }
        }
        catch (IOException x) {
            LOGGER.error("Error analysing archive (not a zip archive?)",x);
        }

        if (entryPoints.size()==0) {
            LOGGER.warn("No entry points extracted from " + webxmlName );
        }
        else {
            LOGGER.info("Entry points extracted from " + webxmlName + " : " + entryPoints.size());
        }


        return new Result(Collections.unmodifiableCollection(entryPoints));
    }


    private void addEntryPoint(String location,Collection<EntryPoint> entryPoints,String className,String methodName,String[] argNames, String category) {
        EntryPoint entryPoint = new EntryPoint(this,location,className, methodName, argNames);
        LOGGER.info("\textracted entry point: " + entryPoint.getClassName() + "#" + methodName + " (" + category + ")");
        entryPoints.add(entryPoint);
    }

    private void addEntryPoint(String location,ClassHierarchy hierarchy,Collection<EntryPoint> entryPoints,String className,String interfaceName,String methodName,String[] argNames, String category) {

        if (hierarchy.isSubtypeOf(className,interfaceName)) {
            EntryPoint entryPoint = new EntryPoint(this,location,className, methodName, argNames);
            LOGGER.info("\textracted entry point: " + entryPoint.getClassName() + "#" + methodName + " (" + category + ")");
            entryPoints.add(entryPoint);
        }
    }

}
