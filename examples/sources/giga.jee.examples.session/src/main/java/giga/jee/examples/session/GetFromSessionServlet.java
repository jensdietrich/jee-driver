package giga.jee.examples.session;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Servlet that uses JEE annotations. Retrieves some values from a session.
 * @author jens dietrich
 */
@WebServlet(name = "PutInSessionServlet", urlPatterns = {"/PutInSessionServlet"})
public class GetFromSessionServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        Object val1 = session.getAttribute("key1");
        Object val2 = session.getAttribute("key2");
        out.println(val1);
        out.println(val2);
        out.close();
    }

    @Override
    public String getServletInfo() {
        return "Example servlet";
    }

}
