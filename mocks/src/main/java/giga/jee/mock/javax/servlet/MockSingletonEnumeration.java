package giga.jee.mock.javax.servlet;

import java.util.Enumeration;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */

public class MockSingletonEnumeration<T> implements Enumeration {
    private boolean touched = false;
    private T element = null;

    public MockSingletonEnumeration(T element) {
        this.element = element;
    }

    public boolean hasMoreElements() {
        return !touched;
    }

    public Object nextElement() {
        touched = true;
        return element;
    }
}
