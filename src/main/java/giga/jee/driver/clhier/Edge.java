package giga.jee.driver.clhier;

import com.google.common.base.Preconditions;

import java.io.Serializable;

/**
 * Custom edge class.
 * @author jens dietrich
 */

public class Edge<V extends Vertex<?>> implements Serializable {

	private static final long serialVersionUID = 8084858521563114358L;
	protected V start = null;
	protected V end = null;

	
	public Edge(V start, V end) {
		super();
        Preconditions.checkNotNull(start,"The start node of an edge cannot be null");
        Preconditions.checkNotNull(end,"The end node of an edge cannot be null");
		this.start = start;
		this.end = end;
	}
	
	public V getStart() {
		return start;
	}
	public V getEnd() {
		return end;
	}

	@Override
	public String toString() {
		
		return new StringBuffer() 			
			.append("P2Edge[")
			.append(this.start.getName())
			.append(" -> ")
			.append(this.end.getName())
			.append("]")
			.toString();
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Edge<?> edge = (Edge<?>) o;

        if (start != null ? !start.equals(edge.start) : edge.start != null) return false;
        return end != null ? end.equals(edge.end) : edge.end == null;

    }

    @Override
    public int hashCode() {
        int result = start != null ? start.hashCode() : 0;
        result = 31 * result + (end != null ? end.hashCode() : 0);
        return result;
    }
}
