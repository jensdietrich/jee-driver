package giga.jee.driver;

import com.google.common.base.Preconditions;
import giga.jee.driver.clhier.ClassHierarchy;
import giga.jee.driver.util.LogSystem;
import org.apache.log4j.Logger;
import java.io.File;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Map;

/**
 * Extract entry points from simple tag classes.
 * @see javax.servlet.jsp.tagext.SimpleTag
 * @author jens dietrich
 */
public class JSPSimpleTagEntryPointExtractor implements EntryPointExtractor {

    private static Logger LOGGER = LogSystem.getLogger(JSPSimpleTagEntryPointExtractor.class);
    private static final String SIMPLE_TAG_SUPERTYPE = "javax.servlet.jsp.tagext.SimpleTag";

    @Override
    public Result extractEntryPoints(File webapp, Map<String,Object> parameters) {
        ClassHierarchy hierarchy = (ClassHierarchy) parameters.get(DriverCompiler.TYPE_HIERARCHY);
        Preconditions.checkArgument(hierarchy!=null);

        LOGGER.info("Extracting entry points");
        Collection<EntryPoint> entryPoints = new LinkedHashSet<>();
        ClassHierarchy.Type root = hierarchy.getVertexByName(SIMPLE_TAG_SUPERTYPE);

        if (root!=null) {
            addEntryPoints(webapp.getName(),entryPoints, root);
        }
        else {
            LOGGER.warn("Root type for jsp simple tags not found: " + SIMPLE_TAG_SUPERTYPE);
        }

        return new Result(entryPoints);
    }

    private void addEntryPoints(String warName,Collection<EntryPoint> entryPoints, ClassHierarchy.Type type) {

        if (!type.isAbstract() && type.isPublic() && !type.getName().startsWith("javax")) {
            entryPoints.add(new EntryPoint(this,warName + "/" + type.getName(),type.getName(),
                new EntryPoint.InvocationSpec("setJspContext","javax.servlet.jsp.JspContext"),
                // skip setParent() -- not sure how to mock this, assume this is top-level
                // skip setters, would have to inspect tld for this, or build better metamodel TODO: investigate
                new EntryPoint.InvocationSpec("setJspBody","javax.servlet.jsp.tagext.JspFragment"),
                new EntryPoint.InvocationSpec("doTag")
            ));
        }

        type.getInEdges().stream()
                .filter(e -> e instanceof ClassHierarchy.Subtype)
                .map(e -> e.getStart())
                .forEach(t -> addEntryPoints(warName,entryPoints,t));
    }
}
