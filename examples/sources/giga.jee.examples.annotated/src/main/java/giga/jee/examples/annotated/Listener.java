package giga.jee.examples.annotated;

import javax.servlet.http.*;
import javax.servlet.annotation.WebListener;

/**
 * Listener that uses annotations for JEE configuration.
 * @author jens dietrich
 */

@WebListener
public class Listener implements HttpSessionListener {


    public void sessionCreated(HttpSessionEvent se) {

    }
    public void	sessionDestroyed(HttpSessionEvent se) {

    }

    private void foo() {
        System.out.println("Session listener called");
    }

}
