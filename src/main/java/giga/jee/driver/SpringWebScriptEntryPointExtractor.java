package giga.jee.driver;

import com.google.common.base.Preconditions;
import giga.jee.driver.clhier.ClassHierarchy;
import giga.jee.driver.util.LogSystem;
import org.apache.log4j.Logger;
import java.io.File;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;

/**
 * Extract entry points for non-abstract subclasses of org.springframework.extensions.webscripts.WebScript.
 * @author jens dietrich
 */
public class SpringWebScriptEntryPointExtractor implements EntryPointExtractor {

    private static Logger LOGGER = LogSystem.getLogger(SpringWebScriptEntryPointExtractor.class);

    private static final String WEB_SCRIPT_SUPERTYPE = "org.springframework.extensions.webscripts.WebScript";

    private static final String[] EXEC_PARAMS = new String[] {
        "org.springframework.extensions.webscripts.WebScriptRequest",
        "org.springframework.extensions.webscripts.WebScriptResponse"
    };

    @Override
    public Result extractEntryPoints(File webapp,Map<String,Object> parameters) {

        LOGGER.info("Extracting entry points");

        ClassHierarchy hierarchy = (ClassHierarchy) parameters.get(DriverCompiler.TYPE_HIERARCHY);
        Preconditions.checkArgument(hierarchy!=null);

        Collection<EntryPoint> entryPoints = new LinkedHashSet<>();
        ClassHierarchy.Type root = hierarchy.getVertexByName(WEB_SCRIPT_SUPERTYPE);

        if (root!=null) {
            addEntryPoints(webapp.getName(),entryPoints, root);
        }
        else {
            LOGGER.warn("Root interface for spring web scripts not found: " + WEB_SCRIPT_SUPERTYPE);
        }

        return new Result(entryPoints);
    }

    private void addEntryPoints(String warName,Collection<EntryPoint> entryPoints, ClassHierarchy.Type type) {

        if (!type.isAbstract() && type.isPublic()) {
            entryPoints.add(new EntryPoint(this,warName + "/" + type.getName(),type.getName(),"execute",EXEC_PARAMS));
        }

        type.getInEdges().stream()
            .filter(e -> e instanceof ClassHierarchy.Subtype)
            .map(e -> e.getStart())
            .forEach(t -> addEntryPoints(warName,entryPoints,t));
    }

}
