package giga.jee.mock.javax.servlet.jsp.tagext;

import giga.jee.mock.javax.servlet.jsp.MockJspContext;
import javax.servlet.jsp.JspContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.JspFragment;
import java.io.IOException;
import java.io.Writer;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */
public class MockJspFragment extends JspFragment {

    public void invoke(Writer writer) throws JspException, IOException {
    }

    public JspContext getJspContext() {
        return new MockJspContext();
    }
}
