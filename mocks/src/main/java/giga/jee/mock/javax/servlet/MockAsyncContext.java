package giga.jee.mock.javax.servlet;

import javax.servlet.*;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */
public class MockAsyncContext implements AsyncContext {

    public ServletRequest getRequest() {
        return new MockServletRequest();
    }

    public ServletResponse getResponse() {
        return new MockServletResponse();
    }

    public boolean hasOriginalRequestAndResponse() {
        return false;
    }

    public void dispatch() {
    }

    public void dispatch(String s) {
    }

    public void dispatch(ServletContext servletContext, String s) {
    }

    public void complete() {

    }

    public void start(Runnable runnable) {
    }

    public void addListener(AsyncListener asyncListener) {
    }

    public void addListener(AsyncListener asyncListener, ServletRequest servletRequest, ServletResponse servletResponse) {
    }

    public <T extends AsyncListener> T createListener(Class<T> aClass) throws ServletException {
        return null;
    }

    public void setTimeout(long l) {

    }

    public long getTimeout() {
        return 0;
    }
}
