package giga.jee.mock.java.io;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */

public class MockOutputStream extends OutputStream {
    public void write(int b) throws IOException {
    }
}
