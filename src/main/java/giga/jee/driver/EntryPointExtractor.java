package giga.jee.driver;

import giga.jee.driver.clhier.ClassHierarchy;
import java.io.File;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Interface for classes extracting entry points from web applications (wars).
 * @author jens dietrich
 */
public interface EntryPointExtractor {

    // see constants in DriverCompiler for parameter keys
    Result extractEntryPoints(File webapp, Map<String,Object> parameters);

    class Result {

        private Collection<EntryPoint> entryPoints = null;
        private Collection<File> generatedSourceFolders = null;

        public Result(Collection<EntryPoint> entryPoints, Collection<File> generatedSourceFolders) {
            this.entryPoints = entryPoints;
            this.generatedSourceFolders = generatedSourceFolders;
        }

        public Result(Collection<EntryPoint> entryPoints) {
            this.entryPoints = entryPoints;
            this.generatedSourceFolders = Collections.EMPTY_LIST;
        }

        public Collection<EntryPoint> getEntryPoints() {
            return entryPoints;
        }
        public Collection<File> getGeneratedSourceFolders() {
            return generatedSourceFolders;
        }

    }

}
