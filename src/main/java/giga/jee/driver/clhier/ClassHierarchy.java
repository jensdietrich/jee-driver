package giga.jee.driver.clhier;

import com.google.common.base.Preconditions;
import giga.jee.driver.util.ASMUtils;
import org.apache.log4j.Logger;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.Opcodes;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Modifier;
import java.util.Collection;

/**
 * A simplified model of relationships between types.
 * Transitive close of supertypes is computed using reflection (i.e. types are resolved against the JRE used to run the program).
 * @author jens dietrich
 */
public class ClassHierarchy extends Graph<ClassHierarchy.Type,ClassHierarchy.Subtype>{

    public static Logger LOGGER = Logger.getLogger(ClassHierarchy.class);

    public static class Type extends Vertex<Subtype> {

        private boolean isAbstract = false;
        private boolean isPublic = false;

        public Type() {}
        public Type(String name,boolean isAbstract,boolean isPublic) {
            super(name);
            this.isAbstract = isAbstract;
            this.isPublic = isPublic;
        }

        public boolean isAbstract () {
            return isAbstract;
        }

        public boolean isPublic () {
            return isPublic;
        }
    }

    public static abstract class Subtype extends Edge<Type> {
        public Subtype(Type start, Type end) {
            super(start, end);
        }
    };

    public static class Extends extends Subtype {
        public Extends(Type start, Type end) {
            super(start, end);
        }
    }

    public static class Implements extends Subtype {
        public Implements(Type start, Type end) {
            super(start, end);
        }
    }

    public ClassHierarchy(Collection<File> jars)  throws IOException {
        this(jars.toArray(new File[jars.size()]));
    }

    public ClassHierarchy(File[] jars)  throws IOException {
        LOGGER.info("Building class hierarchy model");
        // first run, add types
        LOGGER.info("Collecting types");
        for (File jar:jars) {
            ClassVisitor visitor = new ClassVisitor(ASMUtils.ASM_VERSION) {
                @Override
                public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
                    super.visit(version,access,name,signature,superName,interfaces);
                    addType(name,isAbstract(access),isPublic(access));
                }
            };
            ASMUtils.analyse(jar,visitor);
        }
        LOGGER.info("Collected " + this.getVertexCount() + " types");


        LOGGER.info("Collecting extends / implements relationships");
        for (File jar:jars) {
            ClassVisitor visitor = new ClassVisitor(ASMUtils.ASM_VERSION) {
                @Override
                public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
                    super.visit(version,access,name,signature,superName,interfaces);
                    addSubtype(name,superName,true);
                    for (String i:interfaces) {
                        addSubtype(name,i,false);
                    }
                }
            };
            ASMUtils.analyse(jar,visitor);
        }
        LOGGER.info("Collected " + this.getEdgeCount() + " extends / implements relationships");
    }

    private static boolean isAbstract(int flags) {
        return (flags & Opcodes.ACC_ABSTRACT) == Opcodes.ACC_ABSTRACT;
    }

    private static boolean isPublic(int flags) {
        return (flags & Opcodes.ACC_PUBLIC) == Opcodes.ACC_PUBLIC;
    }

    public Type addType(String name,boolean isAbstract,boolean isPublic) {
        name = name.replace('/','.');
        Type type = this.getVertexByName(name);
        if (type==null) {
            type = new Type(name.replace('/', '.'),isAbstract,isPublic);
            add(type);
        }
        return type;
    }

    public boolean addSubtype(String subtypeName,String superTypeName,boolean isExtends) {
        subtypeName = subtypeName.replace('/','.');

        if (superTypeName==null) {
            assert subtypeName.equals("java.lang.Object");
            return false;
        }
        else {
            superTypeName = superTypeName.replace('/', '.');
            Type subtype = this.getVertexByName(subtypeName);
            Preconditions.checkState(subtype != null);
            Type supertype = this.getVertexByName(superTypeName);
            if (supertype == null) {
                // try reflection to resolve
                try {
                    Class clazz = Class.forName(superTypeName);
                    supertype = addTypeViaReflection(clazz);
                } catch (Throwable x) {
                    // create phantom class
                    supertype = addPhantomType(superTypeName);
                }
            }
            return isExtends ? add(new Extends(subtype, supertype)) : add(new Implements(subtype, supertype));
        }
    }

    private Type addTypeViaReflection (Class clazz) {
        Type type = this.getVertexByName(clazz.getName());
        if (type==null) {
            boolean isAbstract = clazz.isInterface() || Modifier.isAbstract(clazz.getModifiers());
            boolean isPublic = Modifier.isPublic(clazz.getModifiers());
            type = addType(clazz.getName(),isAbstract,isPublic);
            Class sup = clazz.getSuperclass();
            if (sup != null) {
                Type supertype = addTypeViaReflection(sup);
                add(new Extends(type,supertype));
            }
            Class[] interfaces = clazz.getInterfaces();
            for (Class ifc:interfaces) {
                Type supertype = addTypeViaReflection(ifc);
                add(new Implements(type,supertype));
            }
        }
        return type;
    }

    private Type addPhantomType(String name) {
        Type type = this.getVertexByName(name);
        if (type==null) {
            LOGGER.debug("Adding phantom type to hierarchy model: " + name);
            return addType(name,true,true);
        }
        else {
            return type;
        }
    }

    // queries

    public boolean isSubtypeOf (String type,String supertype) {
        Type t = this.getVertexByName(type);
        Type st = this.getVertexByName(supertype);

        if (st==null) return false;

        if (t==null && type.startsWith("classes.")) {
            // fix common pattern
            type = type.substring(8);
            t = this.getVertexByName(type);
        }

        if (t==null) {
            LOGGER.warn("Unknown type: " + type);
            return false;
        }
        else {
            return isSubtypeOf(t, st);
        }
    }

    public boolean isProperSubtypeOf (String type,String supertype) {
        if (type.equals(supertype)) return false;
        return isSubtypeOf(type,supertype);
    }

    public boolean isSubtypeOf (Type type,Type superType) {
        if (type.getName().equals(superType.getName())) {
            return true;
        }
        for (Vertex next:type.getSuccessors()) {
            Type nextType = (Type)next;
            if (isSubtypeOf(nextType,superType)) {
                return true;
            }
        }
        return false;
    }
}
