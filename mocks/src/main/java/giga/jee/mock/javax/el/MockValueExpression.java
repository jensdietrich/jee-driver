package giga.jee.mock.javax.el;

import javax.el.ELContext;
import javax.el.ValueExpression;

/**
 * Mock object.
 * @author jens dietrich
 */
public class MockValueExpression extends ValueExpression {

    private ELContext elContext = null;
    private Object value = null;

    public Object getValue(ELContext elContext) {
        return value;
    }

    public void setValue(ELContext elContext, Object o) {
        this.elContext = elContext;
        this.value = value;
    }

    public boolean isReadOnly(ELContext elContext) {
        return false;
    }

    public Class<?> getType(ELContext elContext) {
        return Object.class;
    }

    public Class<?> getExpectedType() {
        return null;
    }

    public String getExpressionString() {
        return null;
    }

    public boolean equals(Object o) {
        return false;
    }

    public int hashCode() {
        return 0;
    }

    public boolean isLiteralText() {
        return false;
    }
}
