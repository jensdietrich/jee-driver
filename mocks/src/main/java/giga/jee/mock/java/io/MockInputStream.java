package giga.jee.mock.java.io;

import java.io.IOException;
import java.io.InputStream;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */

public class MockInputStream extends InputStream {
    public int read() throws IOException {
        return 0;
    }
}
