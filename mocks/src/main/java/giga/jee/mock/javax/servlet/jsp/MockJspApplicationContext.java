package giga.jee.mock.javax.servlet.jsp;

import giga.jee.mock.javax.el.MockExpressionFactory;

import javax.el.ELContextListener;
import javax.el.ELResolver;
import javax.el.ExpressionFactory;
import javax.servlet.jsp.JspApplicationContext;

/**
 * Mock object for jsp application context.
 * @author jens dietrich
 */
public class MockJspApplicationContext implements JspApplicationContext {

    public void addELResolver(ELResolver elResolver) {

    }

    public ExpressionFactory getExpressionFactory() {
        return new MockExpressionFactory();
    }

    public void addELContextListener(ELContextListener elContextListener) {

    }
}
