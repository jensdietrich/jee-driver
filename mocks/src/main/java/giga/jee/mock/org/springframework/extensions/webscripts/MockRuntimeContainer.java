package giga.jee.mock.org.springframework.extensions.webscripts;

import org.springframework.extensions.webscripts.*;
import org.springframework.extensions.webscripts.servlet.WebScriptServletRuntime;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */
public class MockRuntimeContainer extends AbstractRuntimeContainer {


    public MockRuntimeContainer() {
        super();
    }

    public void executeScript(WebScriptRequest webScriptRequest, WebScriptResponse webScriptResponse, Authenticator authenticator) throws IOException {

    }

    public ServerModel getDescription() {
        return null;
    }
}
