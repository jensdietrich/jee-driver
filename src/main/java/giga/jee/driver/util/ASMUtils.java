package giga.jee.driver.util;

import org.objectweb.asm.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Misc utility methods useful to analyse bytecode.
 * @author jens dietrich
 */
public class ASMUtils {

    public static int ASM_VERSION = Opcodes.ASM5;

    public static void analyse (ZipFile zip, ClassVisitor visitor) throws IOException {
        Enumeration<? extends ZipEntry> en = zip.entries();
        while (en.hasMoreElements()) {
            ZipEntry e = en.nextElement();
            String name = e.getName();
            if (name.endsWith(".class")) {
                try (InputStream in = zip.getInputStream(e)) {
                    new ClassReader(in).accept(visitor, 0);
                    in.close();
                }
            }
        }
    }

    public static void analyse (File jar, ClassVisitor visitor) throws IOException {
        analyse(new ZipFile(jar),visitor);
    }

}
