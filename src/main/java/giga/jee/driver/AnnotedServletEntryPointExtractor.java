package giga.jee.driver;

import com.google.common.base.Preconditions;
import giga.jee.driver.clhier.ClassHierarchy;
import giga.jee.driver.util.LogSystem;
import org.apache.log4j.Logger;
import org.objectweb.asm.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 * Extract entry points using servlet 3 (JSR315) annotations.
 * @author jens dietrich
 */
public class AnnotedServletEntryPointExtractor implements EntryPointExtractor {

    private static Logger LOGGER = LogSystem.getLogger(AnnotedServletEntryPointExtractor.class);

    private static final String SERVLET_ANNOTATION = "Ljavax/servlet/annotation/WebServlet;";

    private static final String FILTER_ANNOTATION = "Ljavax/servlet/annotation/WebFilter;";

    private static final String LISTENER_ANNOTATION = "Ljavax/servlet/annotation/WebListener;";



    private class Finder extends ClassVisitor {

        private Collection<EntryPoint> entryPoints = new LinkedHashSet<>();
        private String className = null;
        private String location = null; // some context info, war, path etc
        private ClassHierarchy hierarchy = null;

        public Finder(ClassHierarchy hierarchy) {
            super(Opcodes.ASM5);
            this.hierarchy = hierarchy;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        @Override
        public AnnotationVisitor visitAnnotation(String desc, boolean visible) {
            if (desc.equals(SERVLET_ANNOTATION)) {
                addEntryPoint(location,entryPoints,className,ServletAPISpecs.SERVLET_INIT_METHOD_NAME1, ServletAPISpecs.SERVLET_INIT_METHOD_PARAMS1,"servlet");
                addEntryPoint(location,entryPoints,className,ServletAPISpecs.SERVLET_INIT_METHOD_NAME2, ServletAPISpecs.SERVLET_INIT_METHOD_PARAMS2,"servlet");
                addEntryPoint(location,entryPoints,className,ServletAPISpecs.SERVLET_METHOD_NAME, ServletAPISpecs.SERVLET_METHOD_PARAMS,"servlet");
                addEntryPoint(location,entryPoints,className,ServletAPISpecs.SERVLET_DESTROY_METHOD_NAME, ServletAPISpecs.SERVLET_DESTROY_METHOD_PARAMS,"servlet");

            }
            else if (desc.equals(FILTER_ANNOTATION)) {
                addEntryPoint(location,entryPoints,className,ServletAPISpecs.FILTER_INIT_METHOD_NAME, ServletAPISpecs.FILTER_INIT_METHOD_PARAMS,"filter");
                addEntryPoint(location,entryPoints,className,ServletAPISpecs.FILTER_METHOD_NAME, ServletAPISpecs.FILTER_METHOD_PARAMS,"filter");
                addEntryPoint(location,entryPoints,className,ServletAPISpecs.FIlTER_DESTROY_METHOD_NAME, ServletAPISpecs.FILTER_DESTROY_METHOD_PARAMS,"filter");
            }
            else if (desc.equals(LISTENER_ANNOTATION)) {
                addEntryPoint(location,hierarchy,entryPoints,className,"javax.servlet.ServletContextListener",ServletAPISpecs.SERVLET_CONTEXT_EVENT_METHOD_NAME1, ServletAPISpecs.SERVLET_CONTEXT_EVENT_PARAMS,"listener");
                addEntryPoint(location,hierarchy,entryPoints,className, "javax.servlet.ServletContextListener",ServletAPISpecs.SERVLET_CONTEXT_EVENT_METHOD_NAME2, ServletAPISpecs.SERVLET_CONTEXT_EVENT_PARAMS,"listener");
                addEntryPoint(location,hierarchy,entryPoints,className, "javax.servlet.ServletContextAttributeListener",ServletAPISpecs.SERVLET_CONTEXT_ATTRIBUTE_LISTENER_NAME1, ServletAPISpecs.SERVLET_CONTEXT_ATTRIBUTE_LISTENER_PARAMS,"listener");
                addEntryPoint(location,hierarchy,entryPoints,className, "javax.servlet.ServletContextAttributeListener",ServletAPISpecs.SERVLET_CONTEXT_ATTRIBUTE_LISTENER_NAME2, ServletAPISpecs.SERVLET_CONTEXT_ATTRIBUTE_LISTENER_PARAMS,"listener");
                addEntryPoint(location,hierarchy,entryPoints,className, "javax.servlet.ServletContextAttributeListener",ServletAPISpecs.SERVLET_CONTEXT_ATTRIBUTE_LISTENER_NAME3, ServletAPISpecs.SERVLET_CONTEXT_ATTRIBUTE_LISTENER_PARAMS,"listener");

                addEntryPoint(location,hierarchy,entryPoints,className, "javax.servlet.http.HttpSessionListener",ServletAPISpecs.SERVLET_SESSION_EVENT_METHOD_NAME1, ServletAPISpecs.SERVLET_SESSION_EVENT_PARAMS,"listener");
                addEntryPoint(location,hierarchy,entryPoints,className, "javax.servlet.http.HttpSessionListener",ServletAPISpecs.SERVLET_SESSION_EVENT_METHOD_NAME2, ServletAPISpecs.SERVLET_SESSION_EVENT_PARAMS,"listener");
                addEntryPoint(location,hierarchy,entryPoints,className, "javax.servlet.http.HttpSessionAttributeListener",ServletAPISpecs.SERVLET_SESSION_ATTRIBUTE_LISTENER_NAME1, ServletAPISpecs.SERVLET_SESSION_ATTRIBUTE_LISTENER_PARAMS,"listener");
                addEntryPoint(location,hierarchy,entryPoints,className, "javax.servlet.http.HttpSessionAttributeListener", ServletAPISpecs.SERVLET_SESSION_ATTRIBUTE_LISTENER_NAME2, ServletAPISpecs.SERVLET_SESSION_ATTRIBUTE_LISTENER_PARAMS,"listener");
                addEntryPoint(location,hierarchy,entryPoints,className, "javax.servlet.http.HttpSessionAttributeListener", ServletAPISpecs.SERVLET_SESSION_ATTRIBUTE_LISTENER_NAME3, ServletAPISpecs.SERVLET_SESSION_ATTRIBUTE_LISTENER_PARAMS,"listener");

                addEntryPoint(location,hierarchy,entryPoints,className, "javax.servlet.ServletRequestListener",ServletAPISpecs.SERVLET_REQUEST_EVENT_METHOD_NAME1, ServletAPISpecs.SERVLET_REQUEST_EVENT_PARAMS,"listener");
                addEntryPoint(location,hierarchy,entryPoints,className, "javax.servlet.ServletRequestListener",ServletAPISpecs.SERVLET_REQUEST_EVENT_METHOD_NAME2, ServletAPISpecs.SERVLET_REQUEST_EVENT_PARAMS,"listener");
                addEntryPoint(location,hierarchy,entryPoints,className, "javax.servlet.ServletRequestAttributeListener",ServletAPISpecs.SERVLET_REQUEST_ATTRIBUTE_LISTENER_NAME1, ServletAPISpecs.SERVLET_REQUEST_ATTRIBUTE_LISTENER_PARAMS,"listener");
                addEntryPoint(location,hierarchy,entryPoints,className, "javax.servlet.ServletRequestAttributeListener",ServletAPISpecs.SERVLET_REQUEST_ATTRIBUTE_LISTENER_NAME2, ServletAPISpecs.SERVLET_REQUEST_ATTRIBUTE_LISTENER_PARAMS,"listener");
                addEntryPoint(location,hierarchy,entryPoints,className, "javax.servlet.ServletRequestAttributeListener",ServletAPISpecs.SERVLET_REQUEST_ATTRIBUTE_LISTENER_NAME3, ServletAPISpecs.SERVLET_REQUEST_ATTRIBUTE_LISTENER_PARAMS,"listener");
            }
            return null;
        }

        @Override
        public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
            super.visit(version, access, name, signature, superName, interfaces);
            this.className = name.replace('/','.');
        }

        Collection<EntryPoint> getEntryPoints() {
            return Collections.unmodifiableCollection(entryPoints);
        }
    }

    @Override
    public Result extractEntryPoints(File webapp, Map<String,Object> parameters) {
        Preconditions.checkArgument(webapp.exists(),"File does not exist: " + webapp.getAbsolutePath());
        Preconditions.checkArgument(webapp.getName().endsWith(".war"),"File is not a war file: " + webapp.getAbsolutePath());

        LOGGER.info("Extracting entry points");
        ClassHierarchy hierarchy = (ClassHierarchy) parameters.get(DriverCompiler.TYPE_HIERARCHY);
        Preconditions.checkArgument(hierarchy!=null);

        try (ZipFile zip = new ZipFile(webapp)) {
            Finder finder = new Finder(hierarchy);
            analyse(webapp.getName(),new ZipFile(webapp), finder);

            if (finder.getEntryPoints().size()==0) {
                LOGGER.warn("No entry points extracted");
            }
            else {
                LOGGER.info("Entry points extracted: " + finder.getEntryPoints().size());
            }

            return new Result(finder.getEntryPoints());
        }
        catch (IOException x) {
            LOGGER.error("Error analysing archive (not a zip archive?)",x);
        }

        return null;
    }

    private void analyse (String warName,ZipFile zip, Finder visitor) {
        Enumeration<? extends ZipEntry> en = zip.entries();
        while (en.hasMoreElements()) {
            ZipEntry e = en.nextElement();
            String name = e.getName();
            if (name.startsWith("WEB-INF/classes") && name.endsWith(".class")) {
                try (InputStream in = zip.getInputStream(e)) {
                    String location = warName + "/" + e.getName();
                    LOGGER.debug("analysing entry points in " + location);
                    visitor.setLocation(location);
                    analyse(in, visitor);
                }
                catch (IOException x) {
                    LOGGER.error("Error analysing archive",x);
                }
            }
        }
    }

    private void analyse (InputStream in, ClassVisitor visitor) throws IOException {
        new ClassReader(in).accept(visitor, 0);
    }

    private void addEntryPoint(String location,Collection<EntryPoint> entryPoints,String className,String methodName,String[] argNames, String category) {
        EntryPoint entryPoint = new EntryPoint(this,location,className, methodName, argNames);
        LOGGER.info("\textracted entry point: " + entryPoint.getClassName() + "#" + methodName + " (" + category + ")");
        entryPoints.add(entryPoint);
    }

    private void addEntryPoint(String location,ClassHierarchy hierarchy,Collection<EntryPoint> entryPoints,String className,String interfaceName,String methodName,String[] argNames, String category) {

        if (hierarchy.isSubtypeOf(className,interfaceName)) {
            EntryPoint entryPoint = new EntryPoint(this,location,className, methodName, argNames);
            LOGGER.info("\textracted entry point: " + entryPoint.getClassName() + "#" + methodName + " (" + category + ")");
            entryPoints.add(entryPoint);
        }
    }
}
