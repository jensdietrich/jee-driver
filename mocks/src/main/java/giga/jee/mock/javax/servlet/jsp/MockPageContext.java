package giga.jee.mock.javax.servlet.jsp;

import giga.jee.mock.javax.servlet.*;
import giga.jee.mock.javax.servlet.http.MockHttpServletRequest;
import giga.jee.mock.javax.servlet.http.MockHttpServletResponse;
import giga.jee.mock.javax.servlet.http.MockHttpSession;
import javax.el.ELContext;
import javax.servlet.*;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.PageContext;
import javax.servlet.jsp.el.ExpressionEvaluator;
import javax.servlet.jsp.el.VariableResolver;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */

public class MockPageContext extends PageContext {

    private Map<String,Object> attributes = new HashMap<String,Object>();

    public void setAttribute(String s, Object o) {
        this.attributes.put(s,o);
    }

    public void setAttribute(String s, Object o, int i) {
        this.attributes.put(s,o);
    }

    public Object getAttribute(String s) {
        return this.attributes.get(s);
    }

    public Object getAttribute(String s, int i) {
        return this.attributes.get(s);
    }

    public Object findAttribute(String s) {
        return this.attributes.get(s);
    }

    public void removeAttribute(String s) {
        this.attributes.remove(s);
    }

    public void removeAttribute(String s, int i) {
        this.attributes.remove(s);
    }

    public int getAttributesScope(String s) {
        return 0;
    }

    public Enumeration<String> getAttributeNamesInScope(int i) {
        return new MockSingletonEnumeration("mock attribute name");
    }

    public JspWriter getOut() {
        return new MockJspWriter();
    }

    // @TODO  deeper mocking

    public ExpressionEvaluator getExpressionEvaluator() {
        return null;
    }

    public VariableResolver getVariableResolver() {
        return null;
    }

    public ELContext getELContext() {
        return null;
    }


    public void initialize(Servlet servlet, ServletRequest servletRequest, ServletResponse servletResponse, String s, boolean b, int i, boolean b1) throws IOException, IllegalStateException, IllegalArgumentException {

    }

    public void release() {

    }

    public HttpSession getSession() {
        return new MockHttpSession();
    }

    public Object getPage() {
        return "mock page object";
    }

    public ServletRequest getRequest() {
        return new MockHttpServletRequest();
    }

    public ServletResponse getResponse() {
        return new MockHttpServletResponse();
    }

    public Exception getException() {
        return new Exception();
    }

    public ServletConfig getServletConfig() {
        return new MockServletConfig();
    }

    public ServletContext getServletContext() {
        return new MockServletContext();
    }

    public void forward(String s) throws ServletException, IOException {

    }

    public void include(String s) throws ServletException, IOException {

    }

    public void include(String s, boolean b) throws ServletException, IOException {

    }

    public void handlePageException(Exception e) throws ServletException, IOException {

    }

    public void handlePageException(Throwable throwable) throws ServletException, IOException {

    }
}
