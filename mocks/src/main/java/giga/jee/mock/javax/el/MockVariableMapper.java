package giga.jee.mock.javax.el;

import javax.el.ValueExpression;
import javax.el.VariableMapper;

/**
 * Mock object.
 * @author jens dietrich
 */
public class MockVariableMapper extends VariableMapper {

    public ValueExpression resolveVariable(String s) {
        return new MockValueExpression();
    }

    public ValueExpression setVariable(String s, ValueExpression valueExpression) {
        return new MockValueExpression();
    }
}
