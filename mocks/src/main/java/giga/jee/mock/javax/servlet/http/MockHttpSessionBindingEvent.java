package giga.jee.mock.javax.servlet.http;

import javax.servlet.http.*;
import java.io.IOException;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */
public class MockHttpSessionBindingEvent extends HttpSessionBindingEvent {
    public MockHttpSessionBindingEvent() {
        super(new MockHttpSession(),"a mock session attribute name","a mock session attribute value");
    }
}