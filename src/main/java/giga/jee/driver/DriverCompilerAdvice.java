package giga.jee.driver;

import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Advice for driver compiler, in the sense of AOP.
 * @author jens dietrich
 */
public interface DriverCompilerAdvice {

    void insertCodeBeforeEachInvocation(PrintWriter out, EntryPoint.InvocationSpec spec, String offset) ;

    void insertCodeAfterEachInvocation(PrintWriter out, EntryPoint.InvocationSpec spec,String offset) ;

    void insertCodeBeforeAny(PrintWriter out,AtomicInteger varCounter,String offset);

    void insertCodeAfterAll(PrintWriter out, AtomicInteger varCounter,String offset) ;
}
