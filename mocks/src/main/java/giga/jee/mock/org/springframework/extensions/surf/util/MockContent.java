package giga.jee.mock.org.springframework.extensions.surf.util;

import org.springframework.extensions.surf.util.Content;
import java.io.*;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */
public class MockContent implements Content {

    public MockContent() {
        super();
    }

    public String getContent() throws IOException {
        return null;
    }

    public String getMimetype() {
        return "test/html";
    }

    public String getEncoding() {
        return "UTF-8";
    }

    public long getSize() {
        return 42;
    }

    public InputStream getInputStream() {
        return new ByteArrayInputStream(new byte[1]);
    }

    public Reader getReader() throws IOException {
        return new StringReader("foo");
    }
}
