package giga.jee.mock.javax.servlet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */
public class MockRequestDispatcher implements RequestDispatcher {

    public void forward(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
    }

    public void include(ServletRequest servletRequest, ServletResponse servletResponse) throws ServletException, IOException {
    }
}
