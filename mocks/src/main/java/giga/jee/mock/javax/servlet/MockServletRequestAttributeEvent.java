package giga.jee.mock.javax.servlet;

import javax.servlet.*;
import java.io.IOException;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */
public class MockServletRequestAttributeEvent extends ServletRequestAttributeEvent {
    public MockServletRequestAttributeEvent() {
        super(new MockServletContext(),new MockServletRequest(),"a mock request attribute name","a mock request attribute value");
    }
}