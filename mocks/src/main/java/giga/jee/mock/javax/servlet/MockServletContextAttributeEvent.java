package giga.jee.mock.javax.servlet;

import javax.servlet.*;
import java.io.IOException;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */
public class MockServletContextAttributeEvent extends ServletContextAttributeEvent {
    public MockServletContextAttributeEvent() {
        super(new MockServletContext(),"a mock context attribute name","a mock context attribute value");
    }
}