package giga.jee.mock.org.springframework.extensions.webscripts;

import org.springframework.extensions.webscripts.Authenticator;
import org.springframework.extensions.webscripts.Description;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */
public class MockAuthenticator implements Authenticator {
    public boolean authenticate(Description.RequiredAuthentication requiredAuthentication, boolean b) {
        return false;
    }

    public boolean emptyCredentials() {
        return false;
    }
}
