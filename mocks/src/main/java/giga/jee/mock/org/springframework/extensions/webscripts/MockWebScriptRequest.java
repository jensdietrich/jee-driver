package giga.jee.mock.org.springframework.extensions.webscripts;

import giga.jee.mock.org.springframework.extensions.surf.util.MockContent;
import org.springframework.extensions.surf.util.Content;
import org.springframework.extensions.webscripts.Match;
import org.springframework.extensions.webscripts.WebScriptRequestImpl;

import java.util.HashMap;
import java.util.Map;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */
public class MockWebScriptRequest extends WebScriptRequestImpl {

    public MockWebScriptRequest() {
        super(new MockRuntime());
    }

    private Map<String,String> parameters = new HashMap<String, String>();
    private Map<String,String> headers = new HashMap<String, String>();

    public Match getServiceMatch() {
        return null;
    }

    public String getServerPath() {
        return "/";
    }

    public String getContextPath() {
        return "/";
    }

    public String getServiceContextPath() {
        return "/";
    }

    public String getServicePath() {
        return "/";
    }

    public String getURL() {
        return "http://localhost";
    }

    public String getPathInfo() {
        return ",ock path info";
    }

    public String getQueryString() {
        return "mock-query-string";
    }

    public String[] getParameterNames() {
        return this.parameters.keySet().toArray(new String[this.parameters.size()]);
    }

    public String getParameter(String s) {
        return this.parameters.get(s);
    }

    public String[] getParameterValues(String s) {
        return new String[]{this.parameters.get(s)};
    }

    public String[] getHeaderNames() {
        return this.headers.keySet().toArray(new String[this.headers.size()]);
    }

    public String getHeader(String s) {
        return this.headers.get(s);
    }

    public String[] getHeaderValues(String s) {
        return new String[]{this.headers.get(s)};
    }

    public Content getContent() {
        return new MockContent();
    }

    public String getAgent() {
        return "an agent";
    }
}
