package giga.jee.mock.javax.servlet.jsp;

import javax.servlet.jsp.JspWriter;
import java.io.IOException;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */
public class MockJspWriter extends JspWriter {

    public MockJspWriter() {
        super(42, true);
    }

    public void newLine() throws IOException {

    }

    public void print(boolean b) throws IOException {

    }

    public void print(char c) throws IOException {

    }

    public void print(int i) throws IOException {

    }

    public void print(long l) throws IOException {

    }

    public void print(float v) throws IOException {

    }

    public void print(double v) throws IOException {

    }

    public void print(char[] chars) throws IOException {

    }

    public void print(String s) throws IOException {

    }

    public void print(Object o) throws IOException {

    }

    public void println() throws IOException {

    }

    public void println(boolean b) throws IOException {

    }

    public void println(char c) throws IOException {

    }

    public void println(int i) throws IOException {

    }

    public void println(long l) throws IOException {

    }

    public void println(float v) throws IOException {

    }

    public void println(double v) throws IOException {

    }

    public void println(char[] chars) throws IOException {

    }

    public void println(String s) throws IOException {

    }

    public void println(Object o) throws IOException {

    }

    public void clear() throws IOException {

    }

    public void clearBuffer() throws IOException {

    }

    public void write(char[] cbuf, int off, int len) throws IOException {

    }

    public void flush() throws IOException {

    }

    public void close() throws IOException {

    }

    public int getRemaining() {
        return 0;
    }
}
