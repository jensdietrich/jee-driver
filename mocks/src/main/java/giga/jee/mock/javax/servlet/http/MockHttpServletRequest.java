package giga.jee.mock.javax.servlet.http;

import giga.jee.mock.java.io.MockReader;
import giga.jee.mock.java.security.MockPrincipal;
import giga.jee.mock.javax.servlet.*;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.*;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */
public class MockHttpServletRequest implements HttpServletRequest {

    public MockHttpServletRequest() {
    }

    private HttpSession session = new MockHttpSession();
    private ServletContext context = new MockServletContext();
    private Map<String,String> headers = new HashMap<String, String>();
    private Map<String,String> parameters = new HashMap<String, String>();
    private Map<String,Object> attributes = new HashMap<String, Object>();
    private Map<String,Part> parts = new HashMap<String, Part>();

    public String getAuthType() {
        return "mock auth type";
    }

    public Cookie[] getCookies() {
        return new Cookie[]{new Cookie("mock-cookie-key","mock-cookie-value")};
    }

    public long getDateHeader(String name) {
        return 0;
    }

    public String getHeader(String name) {
        return this.headers.get(name);
    }

    public Enumeration<String> getHeaders(String name) {
        return new MockSingletonEnumeration(getHeader(name));
    }

    public Enumeration<String> getHeaderNames() {
        return Collections.enumeration(this.headers.keySet());
    }

    public int getIntHeader(String name) {
        return 0;
    }

    public String getMethod() {
        return "GET";
    }

    public String getPathInfo() {
        return "mock-path-info";
    }

    public String getPathTranslated() {
        return "mock-path-translated";
    }

    public String getContextPath() {
        return "mock/contyext/path";
    }

    public String getQueryString() {
        return "";
    }

    public String getRemoteUser() {
        return "mock remote user";
    }

    public boolean isUserInRole(String role) {
        return false;
    }

    public Principal getUserPrincipal() {
        return new MockPrincipal();
    }

    public String getRequestedSessionId() {
        return "mock session id";
    }

    public String getRequestURI() {
        return "mock-request-uri";
    }

    public StringBuffer getRequestURL() {
        return new StringBuffer("mock-request-url");
    }

    public String getServletPath() {
        return "mock/servlet/path";
    }

    public HttpSession getSession(boolean create) {
        return new MockHttpSession();
    }

    public HttpSession getSession() {
        return session;
    }

    public String changeSessionId() {
        return "mock-session-id";
    }

    public boolean isRequestedSessionIdValid() {
        return false;
    }

    public boolean isRequestedSessionIdFromCookie() {
        return false;
    }

    public boolean isRequestedSessionIdFromURL() {
        return false;
    }

    public boolean isRequestedSessionIdFromUrl() {
        return false;
    }

    public boolean authenticate(HttpServletResponse response) throws IOException, ServletException {
        return false;
    }

    public void login(String username, String password) throws ServletException {

    }

    public void logout() throws ServletException {

    }

    public Collection<Part> getParts() throws IOException, ServletException {
        return this.parts.values();
    }

    public Part getPart(String name) throws IOException, ServletException {
        return this.parts.get(name);
    }

    public <T extends HttpUpgradeHandler> T upgrade(Class<T> handlerClass) throws IOException, ServletException {
        return null;
    }

    public Object getAttribute(String name) {
        return this.attributes.get(name);
    }

    public Enumeration<String> getAttributeNames() {
        return Collections.enumeration(this.attributes.keySet());
    }

    public String getCharacterEncoding() {
        return "UTF-8";
    }

    public void setCharacterEncoding(String env) throws UnsupportedEncodingException {

    }
    public int getContentLength() {
        return 0;
    }

    public long getContentLengthLong() {
        return 0;
    }

    public String getContentType() {
        return "text/html-mocked";
    }

    public ServletInputStream getInputStream() throws IOException {
        return new MockServletInputStream();
    }

    public String getParameter(String name) {
        return this.parameters.get(name) ;
    }

    public Enumeration<String> getParameterNames() {
        return Collections.enumeration(this.parameters.keySet());
    }

    public String[] getParameterValues(String name) {
        return new String[]{this.parameters.get(name)};
    }

    public Map<String, String[]> getParameterMap() {
        return new HashMap<String, String[]>();
    }

    public String getProtocol() {
        return "mock protocol";
    }

    public String getScheme() {
        return "mock scheme";
    }

    public String getServerName() {
        return "mock server";
    }

    public int getServerPort() {
        return 0;
    }

    public BufferedReader getReader() throws IOException {
        return new BufferedReader(new MockReader());
    }

    public String getRemoteAddr() {
        return "mock.remote.address";
    }

    public String getRemoteHost() {
        return "mock.remote.host";
    }

    public void setAttribute(String name, Object o) {
        this.attributes.put(name,o);
    }

    public void removeAttribute(String name) {
        this.attributes.remove(name);
    }

    public Locale getLocale() {
        return Locale.ENGLISH;
    }

    public Enumeration<Locale> getLocales() {
        return new MockSingletonEnumeration(Locale.ENGLISH);
    }

    public boolean isSecure() {
        return false;
    }

    public RequestDispatcher getRequestDispatcher(String path) {
        return new MockRequestDispatcher();
    }

    public String getRealPath(String path) {
        return "mock/real/path";
    }

    public int getRemotePort() {
        return 0;
    }

    public String getLocalName() {
        return "mock local name";
    }

    public String getLocalAddr() {
        return "mock local address";
    }

    public int getLocalPort() {
        return 0;
    }

    public ServletContext getServletContext() {
        return context;
    }

    public AsyncContext startAsync() throws IllegalStateException {
        return new MockAsyncContext();
    }

    public AsyncContext startAsync(ServletRequest servletRequest, ServletResponse servletResponse) throws IllegalStateException {
        return new MockAsyncContext();
    }

    public boolean isAsyncStarted() {
        return false;
    }

    public boolean isAsyncSupported() {
        return false;
    }

    public AsyncContext getAsyncContext() {
        return new MockAsyncContext();
    }

    public DispatcherType getDispatcherType() {
        return DispatcherType.ASYNC;
    }
}
