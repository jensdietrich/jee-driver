package giga.jee.mock.javax.el;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.MethodExpression;
import javax.el.ValueExpression;

/**
 * Mock object.
 * @author jens dietrich
 */
public class MockExpressionFactory extends ExpressionFactory {

    public ValueExpression createValueExpression(ELContext elContext, String s, Class<?> aClass) {
        return new MockValueExpression();
    }

    public ValueExpression createValueExpression(Object o, Class<?> aClass) {
        return new MockValueExpression();
    }

    public MethodExpression createMethodExpression(ELContext elContext, String s, Class<?> aClass, Class<?>[] classes) {
        return null;
    }

    public Object coerceToType(Object o, Class<?> aClass) {
        return "mock object";
    }
}
