package giga.jee.mock.javax.servlet.http;

import javax.servlet.http.*;
import java.io.IOException;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */
public class MockHttpSessionEvent extends HttpSessionEvent {
    public MockHttpSessionEvent() {
        super(new MockHttpSession());
    }
}