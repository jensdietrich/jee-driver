package giga.jee.driver;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Specification for methods that serve as entries for JEE applications.
 * Example: servlet doGet methods.
 * @author jens dietrich
 */
public class EntryPoint {

    public static class InvocationSpec {
        String methodName = null;
        String[] paramTypeNames = null;
        public InvocationSpec(String methodName, String... paramTypeNames) {
            this.methodName = methodName;
            this.paramTypeNames = paramTypeNames;
        }

        @Override
        public String toString() {
            return "InvocationSpec{" +
                    "methodName='" + methodName + '\'' +
                    ", paramTypeNames=" + Arrays.toString(paramTypeNames) +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            InvocationSpec that = (InvocationSpec) o;

            if (methodName != null ? !methodName.equals(that.methodName) : that.methodName != null) return false;
            // Probably incorrect - comparing Object[] arrays with Arrays.equals
            return Arrays.equals(paramTypeNames, that.paramTypeNames);
        }

        @Override
        public int hashCode() {
            int result = methodName != null ? methodName.hashCode() : 0;
            result = 31 * result + Arrays.hashCode(paramTypeNames);
            return result;
        }
    }

    private String className = null;
    private List<InvocationSpec> invocationSpecs = new ArrayList<>();

    // trace data, can be used in comments
    private EntryPointExtractor extractor = null;
    private String location = null; // jar, followed by callsite, resource etc

    public EntryPoint(EntryPointExtractor extractor, String location, String className, String methodName, String[] paramTypeNames) {
        this.className = className;
        this.invocationSpecs.add(new InvocationSpec(methodName,paramTypeNames));
        this.extractor = extractor;
        this.location = location;
    }

    public EntryPoint(EntryPointExtractor extractor, String location, String className, InvocationSpec... specs) {
        this.className = className;
        this.extractor = extractor;
        this.location = location;
        for (InvocationSpec spec:specs) {
            this.invocationSpecs.add(spec);
        }
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public List<InvocationSpec> getInvocationSpecs() {
        return this.invocationSpecs;
    }


    public EntryPointExtractor getExtractor() {
        return extractor;
    }

    public String getLocation() {
        return location;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EntryPoint that = (EntryPoint) o;

        if (className != null ? !className.equals(that.className) : that.className != null) return false;
        if (invocationSpecs != null ? !invocationSpecs.equals(that.invocationSpecs) : that.invocationSpecs != null)
            return false;
        return location != null ? location.equals(that.location) : that.location == null;
    }

    @Override
    public int hashCode() {
        int result = className != null ? className.hashCode() : 0;
        result = 31 * result + (invocationSpecs != null ? invocationSpecs.hashCode() : 0);
        result = 31 * result + (location != null ? location.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "EntryPoint{" +
                "className='" + className + '\'' +
                ", invocationSpecs=" + invocationSpecs +
                ", location='" + location + '\'' +
                '}';
    }
}
