package giga.jee.examples.session;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 * Servlet that uses JEE annotations. Puts some values into sessions.
 * @author jens dietrich
 */
@WebServlet(name = "PutInSession", urlPatterns = {"/PutInSession"})
public class PutInSession extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("key1","value1");
        session.setAttribute("key2","value2");
    }

    @Override
    public String getServletInfo() {
        return "Example servlet";
    }

}
