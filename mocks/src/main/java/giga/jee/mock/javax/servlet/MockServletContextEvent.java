package giga.jee.mock.javax.servlet;

import javax.servlet.*;
import java.io.IOException;

/**
 * Mock object for driver generation.
 * @author jens dietrich
 */
public class MockServletContextEvent extends ServletContextEvent {
    public MockServletContextEvent() {
        super(new MockServletContext());
    }
}