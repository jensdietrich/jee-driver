package giga.jee.driver;

import com.google.common.base.Preconditions;
import giga.jee.driver.clhier.ClassHierarchy;
import giga.jee.driver.clhier.ClassHierarchyBuilder;
import giga.jee.driver.util.IOUtils;
import giga.jee.driver.util.LogSystem;
import org.apache.commons.cli.*;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipFile;
import static giga.jee.driver.MockConfiguration.*;

/**
 * Generates and compiles a main class that is driving the application.
 * @author jens dietrich
 */
public class DriverCompiler {

    private static Logger LOGGER = LogSystem.getLogger(DriverCompiler.class);


    // context keys
    public static final String TYPE_HIERARCHY = "model.type_hierachy";
    public static final String JRE_JARS = "path.jre";
    public static final String JEE_JARS = "path.jee";
    public static final String CONTAINER_JARS = "path.container";


    public static final String DEFAULT_DRIVER_CLASS_NAME = "JEEDriver";
    public static final String DEFAULT_DRIVER_JAR = "driver.jar";

    private final static class StreamGobbler extends Thread {
        private InputStream is;
        private String name;
        private String task;

        // reads everything from is until empty.
        StreamGobbler(String name,InputStream is,String task) {
            this.is = is;
            this.name = name;
            this.task = task;
        }

        public void run() {
            Logger LOGGER2 = LogSystem.getLogger(DriverCompiler.class.getSimpleName() + "::" + task);
            try {
                InputStreamReader isr = new InputStreamReader(is);
                BufferedReader br = new BufferedReader(isr);
                String line = null;
                while ( (line = br.readLine()) != null) {
                    LOGGER2.info(line);

                }
                LOGGER2.debug("Finished process output redirecting to log: " + name);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    public static final String OPTION_WAR = "war";
    public static final String OPTION_ENTRIES = "entries";
    public static final String OPTION_MOCK_JARS = "mocks";
    public static final String OPTION_JEE_JARS = "jeejars";
    public static final String OPTION_JRE_JARS = "jrejars";
    public static final String OPTION_CONTAINER_JARS = "containerjars";
    public static final String OPTION_OUT = "out";
    public static final String OPTION_HELP = "help";


    static DriverCompilerAdvice advice = new DefaultDriverCompilerAdvice();

    public static void main (String[] args) throws Exception {

        long t1 = System.currentTimeMillis();

        Options options = new Options();

        options.addOption(OPTION_WAR, true, "the location of the web application (a war file) for which to generate the driver (required)");
        options.addOption(OPTION_MOCK_JARS, true, "a comma-separated list of jars containing mock classes for jee apis to be used by the driver (optional, default: lib/mocks)");
        options.addOption(OPTION_JRE_JARS, true, "a comma-separated list of jre platform jars (rt.jar etc) (optional, default: lib/jre)");
        options.addOption(OPTION_JEE_JARS, true, "the location of the jee libs (api, jasper runtime, ..) to be used by the driver (optional, default: lib/jee)");
        options.addOption(OPTION_CONTAINER_JARS, true, "the location of the additional libs usually provided by the container (jstl, ..) to be used by the driver (optional, default: lib/container)");
        options.addOption(OPTION_OUT, true, "the output folder (optional, default: out)");
        options.addOption(OPTION_ENTRIES, false, "if this option is chosen, will list all registered entry point extractors to be used (optional)");
        options.addOption(OPTION_HELP, false, "print options");

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(options, args);

        if (cmd.hasOption(OPTION_HELP)) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java <options> " + DriverCompiler.class.getName(), options);
            System.exit(0);
        }
        else if (cmd.hasOption(OPTION_ENTRIES)) {
            LOGGER.info("Listing active JEE entry point extractor plugins (loaded from metadata via java.util.ServiceLoader):");
            for (EntryPointExtractor epe:getEntryPointExtractors()) {
                LOGGER.info("\t" + epe.getClass().getName());
            }
            return;
        }
        else if (!cmd.hasOption(OPTION_WAR)) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("java <options> " + DriverCompiler.class.getName(), options);
            return;
        }

        String warJarFileName = cmd.getOptionValue(OPTION_WAR);
        File warJar = new File(warJarFileName);
        Preconditions.checkArgument(warJar.exists(),"The war file does not exist: " + warJarFileName);

        String mockJarsFileNames = cmd.hasOption(OPTION_MOCK_JARS)?cmd.getOptionValue(OPTION_MOCK_JARS):"lib/mocks";
        List<File> mockJars = IOUtils.getJars(mockJarsFileNames);

        String jreJarsFileNames = cmd.hasOption(OPTION_JRE_JARS)?cmd.getOptionValue(OPTION_JRE_JARS):"lib/jre";
        List<File> jreJars = IOUtils.getJars(jreJarsFileNames);

        String jeeJarFileNames = cmd.hasOption(OPTION_JEE_JARS)?cmd.getOptionValue(OPTION_JEE_JARS):"lib/jee";
        List<File> jeeJars = IOUtils.getJars(jeeJarFileNames);

        String containerJarFileNames = cmd.hasOption(OPTION_CONTAINER_JARS)?cmd.getOptionValue(OPTION_CONTAINER_JARS):"lib/container";
        List<File> containerJars = IOUtils.getJars(containerJarFileNames);

        List<EntryPointExtractor> extractors = getEntryPointExtractors();
        LOGGER.info("Loaded " + extractors.size() + " entry point extractor plugins");

        File tmp = IOUtils.DEFAULT.createTmpDir("__"+ DriverCompiler.class.getName() + "__"+ warJar.getName());
        File tmpDriverJar = compile(warJar,jreJars,mockJars,jeeJars,containerJars,extractors,tmp);

        // copy to output folder
        String outputFolderName = cmd.hasOption(OPTION_OUT)?cmd.getOptionValue(OPTION_OUT):"out";
        File outputFolder = new File(outputFolderName);
        Preconditions.checkArgument(!outputFolder.exists() || outputFolder.isDirectory());
        if (!outputFolder.exists()) {
            outputFolder.mkdirs();
            LOGGER.info("Created output folder " + outputFolder.getAbsolutePath());
        }
        File driverJar = new File(outputFolder,DEFAULT_DRIVER_JAR);
        FileUtils.copyFile(tmpDriverJar,driverJar);

        long t2 = System.currentTimeMillis();

        LOGGER.info("Driver created, jar file written to " + driverJar.getAbsolutePath());
        LOGGER.info("Temporary files including driver sources written to " + tmp.getAbsolutePath());
        LOGGER.info("Generation took " + (t2-t1) + "ms");
    }


    /**
     * Compiles a webapp (war), and returns a jar that contains a compiled driver class that contains a main
     * method invoking the
     * @throws Exception
     */
    public static File compile (File war,List<File> jreJars,List<File> mockJars, List<File> jeeJars,List<File> containerJars,List<EntryPointExtractor> extractors,File tmp) throws Exception {
        Preconditions.checkArgument(extractors.size()>0,"No entry point extractors found");
        Preconditions.checkState(war.exists(),"Application does not exist: " + war.getAbsolutePath());

        List<File> jarsToResolve = new ArrayList<>();
        jarsToResolve.addAll(jreJars);
        jarsToResolve.addAll(jeeJars);
        jarsToResolve.addAll(containerJars);
        jarsToResolve.addAll(mockJars);

        ClassHierarchy typeHierarchy = new ClassHierarchyBuilder().buildMetaModel(war,jarsToResolve);
        Map<String,Object> parameters = new HashMap<>();
        parameters.put(TYPE_HIERARCHY,typeHierarchy);
        parameters.put(JRE_JARS,jreJars);
        parameters.put(JEE_JARS,jeeJars);
        parameters.put(CONTAINER_JARS,containerJars);

        Collection<EntryPoint> entryPoints = new LinkedHashSet<>();
        Collection<File> sourceFolders = new LinkedHashSet<>();




        for (EntryPointExtractor extractor:extractors) {
            EntryPointExtractor.Result result = extractor.extractEntryPoints(war,parameters);
            entryPoints.addAll(result.getEntryPoints());
            sourceFolders.addAll(result.getGeneratedSourceFolders());
        }

        for (EntryPoint entryPoint:entryPoints) {
            for (EntryPoint.InvocationSpec spec:entryPoint.getInvocationSpecs()) {
                for (String paramTypeName : spec.paramTypeNames) {
                    Preconditions.checkArgument(MOCKS.containsKey(paramTypeName), "No known mock object for type: " + paramTypeName);
                }
            }
        }

        LOGGER.info("Assembling files for fact extraction and analysis in " + tmp.getAbsolutePath());
        File tmpSrc = new File(tmp,"src");
        if (tmpSrc.exists()) {
            FileUtils.deleteDirectory(tmpSrc);
        }
        else {
            tmpSrc.mkdirs();
        }

        LOGGER.info("Creating driver");
        File driverSrc = new File(tmpSrc, DEFAULT_DRIVER_CLASS_NAME +".java");
        try (PrintWriter out = new PrintWriter(new FileWriter(driverSrc))) {
            createDriverSourceCode(entryPoints,out);
            LOGGER.info("\tdriver sources created: " + driverSrc.getAbsolutePath());
        }

        LOGGER.info("Unpacking jar and appending content to classpath");
        ZipFile zip = new ZipFile(war);
        File tmpLib = new File(tmp,"lib");
        tmpLib.mkdirs();
        File tmpClasses = new File(tmp,"classes");
        tmpClasses.mkdirs();
        zip.stream().forEach(
            entry -> {
                String n = entry.getName();
                if (isValidZipEntryName(n)) {
                    if (n.endsWith(".jar")) {
                        try (InputStream in = zip.getInputStream(entry)) {
                            LOGGER.info("\tExtracting jar " + n + " from " + war.getAbsolutePath());
                            String[] parts = n.split("/");
                            Files.copy(in, Paths.get(tmpLib.getAbsolutePath(), parts[parts.length - 1]));
                        } catch (IOException x) {
                            LOGGER.error("Error extracting jar " + n + " from " + war.getAbsolutePath(), x);
                        }
                    }
                    if (n.startsWith("WEB-INF/classes/") && n.endsWith(".class")) {
                        try (InputStream in = zip.getInputStream(entry)) {
                            String className = n.substring("WEB-INF/classes/".length());
                            Path path = Paths.get(tmpClasses.getAbsolutePath(), className);
                            Files.createDirectories(path.getParent());
                            Files.copy(in, path);
                        } catch (IOException x) {
                            LOGGER.error("Error extracting jar " + n + " from " + war.getAbsolutePath(), x);
                        }
                    }
                }
            }
        );

        // copy sources generated by entry points extractions (e.g. source code of servlets generated from jsps)
        // into driver src folder to be compiled
        for (File srcFolder:sourceFolders) {
            FileUtils.copyDirectory(srcFolder,driverSrc.getParentFile());
        }

        // copying mocks into same folder
        for (File mockJar:mockJars) {
            File mockCopy = new File(tmpLib, "__" + mockJar.getName());
            LOGGER.info("Adding jee mock lib to " + mockCopy.getAbsolutePath());
            FileUtils.copyFile(mockJar, mockCopy);
        }

        File driverJar = new File(tmp, DEFAULT_DRIVER_JAR);

        LOGGER.info("Building driver jar");
        javacAndJar(tmpSrc,tmpLib,tmpClasses,driverJar,jeeJars,containerJars);

        return driverJar;
    }

    private static boolean isValidZipEntryName(String n) {
        return n!=null && !n.startsWith("__MACOSX");
    }

    private static void javacAndJar(File src, File libFolderFromWebapp, File classesFolderFromWebapp, File driverJar, List<File> jeeJars, List<File> containerJars) throws Exception {

        LOGGER.info("Compiling generated sources (javac must be in path !)");

        List<String> command = new ArrayList<>();

        // TODO check whether javacAndJar exists (better to run this via ant ?)
        command.add("javac");

        String SEP = File.pathSeparator;

        String cp = jeeJars.stream().map(f -> f.getAbsolutePath()).collect(Collectors.joining(SEP));
        cp = cp + SEP + containerJars.stream().map(f -> f.getAbsolutePath()).collect(Collectors.joining(SEP));
        cp = cp + SEP + Stream.of(libFolderFromWebapp.listFiles()).map(f -> f.getAbsolutePath()).collect(Collectors.joining(SEP)) + SEP + classesFolderFromWebapp.getAbsolutePath();
        LOGGER.info("Using compilation classpath: " + cp);
        command.add("-cp");
        command.add(cp);

        command.add("-source");
        command.add("1.8");

        command.add("-target");
        command.add("1.8");

        command.add("-sourcepath");
        command.add(src.getAbsolutePath());
        File bin = new File(src.getParentFile(),"bin");
        bin.mkdirs();
        command.add("-d");
        command.add(bin.getAbsolutePath());
        command.add(src.getAbsolutePath() + "/" + DEFAULT_DRIVER_CLASS_NAME +".java");



        ProcessBuilder builder = new ProcessBuilder(command);
        Process process = builder.start();
        StreamGobbler errorGobbler = new StreamGobbler("err",process.getErrorStream(),"javac");
        StreamGobbler outputGobbler = new StreamGobbler("out",process.getInputStream(),"javac");
        errorGobbler.start();
        outputGobbler.start();
        int status = process.waitFor();
        if (status==0) {
            LOGGER.info("Successfully compiled driver and mocks used by driver, classes are in " + bin.getAbsolutePath());
        }
        else {
            LOGGER.fatal("Compilation of driver has failed, check log for javac output");
            System.exit(1);
        }


        LOGGER.info("Packaging compiled classes (jar must be in path !)");

        LOGGER.info("Creating driver jar");
        command = new ArrayList<>();
        command.add("jar");
        command.add("-cvf");
        command.add(driverJar.getAbsolutePath());
        command.add("-C");
        command.add(bin.getAbsolutePath());// + "/*");
        command.add(".");

//        for (String s:command) {
//            System.out.println(s);
//        }

        builder = new ProcessBuilder(command);
        process = builder.start();
        errorGobbler = new StreamGobbler("err",process.getErrorStream(),"jar");
        outputGobbler = new StreamGobbler("out",process.getInputStream(),"jar");
        errorGobbler.start();
        outputGobbler.start();
        status = process.waitFor();
        if (status==0) {
            LOGGER.info("Successfully packaged driver");
        }
        else {
            LOGGER.fatal("Packaging of driver has failed");
            throw new IllegalStateException();
        }
        LOGGER.info("Successfully build driver jar: " + driverJar.getAbsolutePath());

    }


    private static void createDriverSourceCode(Collection<EntryPoint> entryPoints,PrintWriter out) throws Exception {

        Map<String,String> instanceNames = new HashMap();

        out.println("// this is a generated class to invoke JEE entry methods");
        out.println("// mock objects will be used as parameters");
        out.println("// generator: " + DriverCompiler.class.getName());
        out.println();
        out.println("public final class " + DEFAULT_DRIVER_CLASS_NAME + " {");
        out.println();

        // out.println("// also create alternative static entry point");
        out.println("\tpublic static void main (String[] args) throws Exception {");
        out.println("\t\tnew " + DEFAULT_DRIVER_CLASS_NAME + "().entry();");
        out.println("\t}");
        out.println();

        out.println("\tpublic void entry () throws Exception {");

        AtomicInteger c = new AtomicInteger(0);
        
        advice.insertCodeBeforeAny(out,c,"\t\t");

        for (EntryPoint entryPoint:entryPoints) {
            out.println();

            // metadata
            if (entryPoint.getExtractor()!=null) {
                out.println("\t\t// extracted by: " + entryPoint.getExtractor().getClass().getName());
            }
            if (entryPoint.getLocation()!=null) {
                out.println("\t\t// source location: " + entryPoint.getLocation());
            }

            String className = entryPoint.getClassName();
            String instanceName = instanceNames.get(className);
            if (instanceName==null) {
                instanceName = "_i" + (c.incrementAndGet());
                instanceNames.put(className,instanceName);
                out.println("\t\t" + className + " " + instanceName + " = new " + entryPoint.getClassName() + "();");
            }

            for (EntryPoint.InvocationSpec spec:entryPoint.getInvocationSpecs()){
                advice.insertCodeBeforeEachInvocation(out,spec,"\t\t");
                out.print("\t\t" + instanceName + "." + spec.methodName + "(");
                out.print(String.join(",", (List<String>) Stream.of(spec.paramTypeNames).map(t -> MOCKS.get(t)).collect(Collectors.toList())));
                out.println(");");
                advice.insertCodeAfterEachInvocation(out,spec,"\t\t");
            }
        }
        out.println();

        advice.insertCodeAfterAll(out,c,"\t\t");

        out.println("\t}");
        out.println("}");
    }

//    private static void insertCodeBeforeInvocation(PrintWriter out, EntryPoint.InvocationSpec spec,String offset) {
//        out.print(offset);
//        out.println("try {");
//    }
//
//    private static void insertCodeAfterInvocation(PrintWriter out, EntryPoint.InvocationSpec spec,String offset) {
//        out.print(offset);
//        out.println("\tSystem.out.println(\"ok\");");
//        out.print(offset);
//        out.println("} catch (Throwable _t) {");
//        out.print(offset);
//        out.println("\tSystem.out.println(\"error: \" + _t);");
//        out.print(offset);
//        out.println("}");
//    }
//
//    private static void prePrintDriverMethod(AtomicInteger varCounter, PrintWriter out) {
//
//        // global mocking
//        out.println();
//        out.println("\t\tjava.sql.DriverManager.registerDriver(" + MOCKS.get(java.sql.Driver.class.getName()) + ");");
//        out.println();
//
//    }
//
//    private static void postPrintDriverMethod(AtomicInteger varCounter, PrintWriter out) {
//    }


    private static List<EntryPointExtractor> getEntryPointExtractors() {
        Class service = EntryPointExtractor.class;
        ServiceLoader<EntryPointExtractor> loader = ServiceLoader.load(service);
        List<EntryPointExtractor> entryPointExtractors = new ArrayList<>();
        for (Iterator<EntryPointExtractor> iter = loader.iterator();iter.hasNext();) {
            entryPointExtractors.add(iter.next());
        }
        return entryPointExtractors;
    }

}
